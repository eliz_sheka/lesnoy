<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PastimeImages extends Model
{
    protected $fillable = ['title_ua', 'title_ru', 'image'];
}
