<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Header extends Model
{
    protected $fillable = ['title_ru', 'title_en', 'text_ru', 'text_en', 'description_ru', 'description_en', 'address_ru', 'address_en', 'phone'];
}
