<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pastime extends Model
{
    protected $fillable = ['title_ua', 'title_ru'];
}
