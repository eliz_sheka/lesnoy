<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['name_ua', 'name_ru', 'info_ua', 'info_ru'];

    public function room_images() {
        return $this->hasMany('App\RoomImage');
    }
}
