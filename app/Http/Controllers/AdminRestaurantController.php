<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Restaurant;
use App\RestaurantItem;
use Validator;
use Illuminate\Support\Facades\Input;

class AdminRestaurantController extends Controller
{
    public function restaurant() {
        $restaurant = Restaurant::first();
        $restaurant_items = RestaurantItem::all();
//        dd($restaurant);
        return view('admin.restaurant_block.restaurant',compact('restaurant','restaurant_items'));
    }
    public function create_restaurant_title(Request $request) {
//        dd($request);
        $validator = Validator::make($request->all(), [
            'menu_ua' => 'mimes:doc,docx,txt,xls,xlsx,pdf,ppt,pptx,jpeg,jpg,zip,png,gif|max:20000',
            'menu_ru' => 'mimes:doc,docx,txt,xls,xlsx,pdf,ppt,pptx,jpeg,jpg,zip,png,gif|max:20000'
        ])->validate();
        $restaurant = new Restaurant;
        $restaurant->title_ua = $request->title_ua;
        $restaurant->title_ru = $request->title_ru;
        if ($request->file()) {
            if ($input = Input::file('menu_ua')) {
                $menu_ua = $request->file()['menu_ua'];
                $menu_uaName = 'menu_ua'.'.'.$menu_ua->getClientOriginalExtension();
                $menu_ua->move(storage_path('app/public/menu'), $menu_uaName);
                $extension = $menu_ua->getClientOriginalExtension();
                $restaurant->img_ua = $extension.'.png';
                $restaurant->menu_ua = $menu_uaName;

            }
            if ($inputf = Input::file('menu_ru')) {
                $menu_ru = $request->file()['menu_ru'];
                $menu_ruName = 'menu_ru'.'.'.$menu_ru->getClientOriginalExtension();
                $menu_ru->move(storage_path('app/public/menu'), $menu_ruName);
                $extension = $menu_ru->getClientOriginalExtension();
                $restaurant->img_ru = $extension.'.png';
                $restaurant->menu_ru = $menu_ruName;
            }
        }
//        dd($restaurant);
        $restaurant->save();

        return back()->with('success', 'Успешно добавлено!');
    }
    public function edit_restaurant_title() {
        $restaurant = Restaurant::first();
        return view('admin.restaurant_block.edit_restaurant_title',compact('restaurant'));
    }
    public function update_restaurant_title(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'menu_ua' => 'mimes:doc,docx,txt,xls,xlsx,pdf,ppt,pptx,jpeg,jpg,zip,png,gif|max:20000',
            'menu_ru' => 'mimes:doc,docx,txt,xls,xlsx,pdf,ppt,pptx,jpeg,jpg,zip,png,gif|max:20000'
        ])->validate();
        $restaurant=Restaurant::find($id);
        $restaurant->title_ua = $request->title_ua;
        $restaurant->title_ru = $request->title_ru;
        if ($request->file()) {
            if ($input = Input::file('menu_ua')) {
                $filename=$restaurant->menu_ua;
                if (!empty($filename)) {
                    unlink(storage_path('app/public/menu/').$filename);
                }
                $menu_ua = $request->file()['menu_ua'];
                $menu_uaName = 'menu_ua'.'.'.$menu_ua->getClientOriginalExtension();
                $menu_ua->move(storage_path('app/public/menu'), $menu_uaName);
                $extension = $menu_ua->getClientOriginalExtension();
                $restaurant->img_ua = $extension.'.png';
                $restaurant->menu_ua = $menu_uaName;
            }
            if ($inputf = Input::file('menu_ru')) {
                $filename=$restaurant->menu_ru;
                if (!empty($filename)) {
                    unlink(storage_path('app/public/menu/').$filename);
                }
                $menu_ru = $request->file()['menu_ru'];
                $menu_ruName = 'menu_ru'.'.'.$menu_ru->getClientOriginalExtension();
                $menu_ru->move(storage_path('app/public/menu'), $menu_ruName);
                $extension = $menu_ru->getClientOriginalExtension();
                $restaurant->img_ru = $extension.'.png';
                $restaurant->menu_ru = $menu_ruName;

            }
        }
        $restaurant->save();
        return redirect('/admin/restaurant')->with('success', 'Успешно изменено!');
    }
    public function delete_menu_ua ($id) {
        $restaurant=Restaurant::find($id);
        $filename=$restaurant->menu_ua;
        unlink(storage_path('app/public/menu/').$filename);
        $restaurant->menu_ua='';
        $restaurant->img_ua='';
        $restaurant->save();
        return redirect()->back();
    }
    public function delete_menu_ru ($id) {
        $restaurant=Restaurant::find($id);
        $filename=$restaurant->menu_ru;
        unlink(storage_path('app/public/menu/').$filename);
        $restaurant->menu_ru='';
        $restaurant->img_ru='';
        $restaurant->save();
        return redirect()->back();
    }


    public function create_restaurant_item(Request $request) {
//        dd($request);
        $exist=RestaurantItem::where('position','=',$request->position)->first();
        if (!empty($exist)) {
            $exist->position=null;
            $exist->save();
        }
        $restaurant_items = new RestaurantItem;
        $restaurant_items->title_ua = $request->title_ua;
        $restaurant_items->title_ru = $request->title_ru;
        $restaurant_items->text_ua = $request->text_ua;
        $restaurant_items->text_ru = $request->text_ru;
        $restaurant_items->position = $request->position;
        if ($request->file()) {
            $image = $request->file()['image'];
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $image->move(storage_path('app/public/images/restaurant_item'), $imageName);
            $restaurant_items->image = $imageName;
        }
        $restaurant_items->save();

        return back()->with('success', 'Успешно добавлено!');
    }
    public function edit_restaurant_item($id) {
        $restaurant_item = RestaurantItem::find($id);
        return view('admin.restaurant_block.edit_restaurant_item',compact('restaurant_item'));
    }
    public function update_restaurant_item(Request $request, $id) {
        $restaurant_items=RestaurantItem::find($id);
        $exist=RestaurantItem::where('id','!=',$restaurant_items->id)->where('position','=',$request->position)->first();
//        dd($exist);
        if (!empty($exist)) {
            $exist->position=null;
            $exist->save();
        }
        $restaurant_items->title_ua = $request->title_ua;
        $restaurant_items->title_ru = $request->title_ru;
        $restaurant_items->text_ua = $request->text_ua;
        $restaurant_items->text_ru = $request->text_ru;
        $restaurant_items->position = $request->position;
        if ($request->file()) {
            $image = $request->file()['image'];
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $image->move(storage_path('app/public/images/restaurant_item'), $imageName);
            $restaurant_items->image = $imageName;
        }
        $restaurant_items->save();
        return redirect('/admin/restaurant')->with('success', 'Успешно изменено!');
    }
    public function delete_image ($id) {
        $restaurant_item=RestaurantItem::find($id);
        $filename=$restaurant_item->image;
        unlink(storage_path('app/public/images/restaurant_item/').$filename);
        $restaurant_item->image='';
        $restaurant_item->save();
        return redirect()->back();
    }
}
