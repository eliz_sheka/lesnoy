<?php

namespace App\Http\Controllers;

use App\HeaderImage;
use Illuminate\Http\Request;
use App\Apartment;
use App\Contact;
use App\FooterText;
use App\Gallery;
use App\GalleryImage;
use App\Header;
use App\Kids;
use App\KidsImage;
use App\Link;
Use App\Pastime;
use App\PastimeImages;
use App\Restaurant;
use App\RestaurantItem;
use App\Room;
use App\RoomImage;

class PageController extends Controller
{
    public function index() {
        $apartment = Apartment::first();
        $contact = Contact::first();
        $footer = FooterText::first();
        $gallery = Gallery::first();
        $gallery_images = GalleryImage::get();
//        dd($gallery_images);
        $header = Header::first();
//        dd($header);
        $header_images = HeaderImage::take(3)->get();
//        dd($header_images);
        $kid = Kids::first();
        $kid_images = KidsImage::all();
//        dd($kid_images);
        $links = Link::all();
        $pastime = Pastime::first();
        $pastime_images = PastimeImages::all();
        $restaurant = Restaurant::first();
        $restaurant_item1 = RestaurantItem::where('position','=','1')->first();
        $restaurant_item2 = RestaurantItem::where('position','=','2')->first();
        $restaurant_item3 = RestaurantItem::where('position','=','3')->first();
//        dd($restaurant_item1);
        $rooms = Room::all();
//        dd($rooms);
        foreach ($rooms as $room) {
            $room->images = $room->room_images;
        }

        return view('index', compact('apartment', 'contact', 'footer',
            'gallery', 'gallery_images', 'header', 'header_images', 'kid', 'kid_images',
            'links', 'pastime', 'pastime_images', 'restaurant', 'restaurant_item1', 'restaurant_item2', 'restaurant_item3', 'rooms'));
    }

    public function index_ua() {
        $apartment = Apartment::first();
        $contact = Contact::first();
        $footer = FooterText::first();
        $gallery = Gallery::first();
        $gallery_images = GalleryImage::get();
//        dd($gallery_images);
        $header = Header::first();
//        dd($header);
        $header_images = HeaderImage::take(3)->get();
//        dd($header_images);
        $kid = Kids::first();
        $kid_images = KidsImage::all();
//        dd($kid_images);
        $links = Link::all();
        $pastime = Pastime::first();
        $pastime_images = PastimeImages::all();
        $restaurant = Restaurant::first();
        $restaurant_item1 = RestaurantItem::where('position','=','1')->first();
        $restaurant_item2 = RestaurantItem::where('position','=','2')->first();
        $restaurant_item3 = RestaurantItem::where('position','=','3')->first();
//        dd($restaurant_item1);
        $rooms = Room::all();
//        dd($rooms);
        foreach ($rooms as $room) {
            $room->images = $room->room_images;
        }

        return view('index_ua', compact('apartment', 'contact', 'footer',
            'gallery', 'gallery_images', 'header', 'header_images', 'kid', 'kid_images',
            'links', 'pastime', 'pastime_images', 'restaurant', 'restaurant_item1', 'restaurant_item2', 'restaurant_item3', 'rooms'));
    }
}
