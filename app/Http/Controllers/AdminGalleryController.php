<?php

namespace App\Http\Controllers;

use App\GalleryImage;
use Illuminate\Http\Request;
use App\Gallery;

class AdminGalleryController extends Controller
{
    public function gallery() {
        $gallery = Gallery::first();
        $gallery_image = GalleryImage::all();
//        dd($gallery_image);
        return view('admin.gallery_block.gallery', compact('gallery','gallery_image'));
    }

    public function create_gallery(Request $request) {

//        dd($request);
        $data = $request->except('_token');
//        dd($data);
        $gallery = new Gallery;
        $gallery->title_ru=$request->title_ru;
        $gallery->title_ua=$request->title_ua;
        $gallery->save();
        $images = $request->file();
        if($images){
            foreach ($images['image'] as $image) {
                $gallery_image = new GalleryImage;
                $imageName = time().sha1($image).'.'.$image->getClientOriginalExtension();
                $image->move(storage_path('app/public/images/gallery'), $imageName);
                $gallery_image->image  = $imageName;
                $gallery_image->save();
            }
        }
        return back()->with('success', 'Успешно добавлено!');
    }
    public function edit_gallery() {
        $gallery = Gallery::first();
        $gallery_image = GalleryImage::all();
//        dd($kids);
        return view('admin.gallery_block.edit_gallery', compact('gallery','gallery_image'));
    }
    public function update_gallery(Request $request, $id) {
//        dd($request);
        $gallery = Gallery::find($id);
        $gallery->title_ru=$request->title_ru;
        $gallery->title_ua=$request->title_ua;
        $gallery->save();
        $images = $request->file();
        if($images){
            foreach ($images['image'] as $image) {
                $gallery_image = new GalleryImage;
                $imageName = time().sha1($image).'.'.$image->getClientOriginalExtension();
                $image->move(storage_path('app/public/images/gallery'), $imageName);
                $gallery_image->image  = $imageName;
                $gallery_image->save();
            }
        }

        return redirect('/admin/gallery')->with('success', 'Успешно изменено!');
    }

    public function delete_image ($id) {
        $gallery=GalleryImage::find($id)->delete();
        return redirect()->back();
    }
}
