<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pastime;
use App\PastimeImages;

class AdminPastimeController extends Controller
{
    public function pastime() {
        $pastime = Pastime::first();
        $pastime_images=PastimeImages::all();
//        dd($header);
        return view('admin.pastime_block.pastime', compact('pastime','pastime_images'));
    }

    public function create_pastime(Request $request) {

//        dd($request);
        $data = $request->except('_token');
//        dd($data);
        $pastime = new Pastime;
        $pastime->title_ru=$request->title_ru;
        $pastime->title_ua=$request->title_ua;
        $pastime->text_ru=$request->text_ru;
        $pastime->text_ua=$request->text_ua;
        $pastime->save();

        return back()->with('success', 'Успешно добавлено!');
    }
    public function edit_pastime() {
        $pastime = Pastime::first();
//        dd($kids);
        return view('admin.pastime_block.edit_pastime', compact('pastime'));
    }
    public function update_pastime(Request $request, $id) {

        $pastime = Pastime::find($id);
        $pastime->title_ru=$request->title_ru;
        $pastime->title_ua=$request->title_ua;
        $pastime->text_ru=$request->text_ru;
        $pastime->text_ua=$request->text_ua;
        $pastime->save();
        return redirect('/admin/pastime')->with('success', 'Успешно изменено!');
    }

    public function create_pastime_image(Request $request) {

//        dd($request);
        $data = $request->except('_token');
//        dd($data);
        $pastime_images = new PastimeImages();
        $pastime_images->title_ru=$request->title_ru;
        $pastime_images->title_ua=$request->title_ua;
        $image = $request->file()['image'];
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $image->move(storage_path('app/public/images/pastime'), $imageName);
        $pastime_images->image = $imageName;
        $pastime_images->save();

        return back()->with('success', 'Успешно добавлено!');
    }
    public function edit_pastime_image($id) {
        $pastime_image = PastimeImages::find($id);
//        dd($kids);
        return view('admin.pastime_block.edit_pastime_image', compact('pastime_image'));
    }
    public function update_pastime_image(Request $request, $id) {

        $pastime_images = PastimeImages::find($id);
//        dd($pastime_images);
        $pastime_images->title_ru=$request->title_ru;
        $pastime_images->title_ua=$request->title_ua;
        if ($request->file()) {
            $image = $request->file()['image'];
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $image->move(storage_path('app/public/images/pastime'), $imageName);
            $pastime_images->image = $imageName;
        }
        $pastime_images->save();
        return redirect('/admin/pastime')->with('success', 'Успешно изменено!');
    }
    public function delete_pastime_image ($id) {
        $pastime_images=PastimeImages::find($id);
        $filename=$pastime_images->image;
        unlink(storage_path('app/public/images/pastime/').$filename);
        $pastime_images->image='';
        $pastime_images->save();
        return redirect()->back();
    }
    public function delete_pastime ($id) {
        $pastime_images=PastimeImages::find($id)->delete();
        return redirect()->back();
    }
}
