<?php

namespace App\Http\Controllers;

use App\HeaderImage;
use Illuminate\Http\Request;
use App\Header;
use Validator;

class AdminController extends Controller
{
    public function index() {
        $header = Header::first();
        $header_images = HeaderImage::all();
//        dd($header);
        return view('admin.header_block.header', compact('header', 'header_images'));
    }

    public function create_header(Request $request) {
        $validator = Validator::make($request->all(), [
            'title_ru' => 'required| max:255',
            'title_ua' => 'required| max:255',
            'text_ru' => 'required',
            'text_ua' => 'required',
            'description_ru' => 'required',
            'description_ua' => 'required',
            'address_ua' => 'required',
            'address_ru' => 'required',
        ])->validate();
//        dd($request);
        $data = $request->except('_token');
//        dd($data);
        $header = new Header;
        $header->title_ru=$request->title_ru;
        $header->title_ua=$request->title_ua;
        $header->text_ru=$request->text_ru;
        $header->text_ua=$request->text_ua;
        $header->description_ru=$request->description_ru;
        $header->description_ua=$request->description_ua;
        $header->address_ru=$request->address_ru;
        $header->address_ua=$request->address_ua;
        $header->phone=$request->phone;
        $header->save();
        $images = $request->file();
        if($images){
            foreach ($images['image'] as $image) {
                $header_image = new HeaderImage;
                $imageName = time().sha1($image).'.'.$image->getClientOriginalExtension();
                $image->move(storage_path('app/public/images/header'), $imageName);
                $header_image->image  = $imageName;
                $header_image->save();
            }
        }

        return back()->with('success', 'Успешно добавлено!');
    }
    public function edit_header() {
        $header = Header::first();
        $header_images = HeaderImage::all();
        return view('admin.header_block.edit_header', compact('header', 'header_images'));
    }
    public function update_header(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'title_ru' => 'required| max:255',
            'title_ua' => 'required| max:255',
            'text_ru' => 'required',
            'text_ua' => 'required',
            'description_ru' => 'required',
            'description_ua' => 'required',
            'address_ua' => 'required',
            'address_ru' => 'required',
        ])->validate();

        $header = Header::find($id);
        $header->title_ru=$request->title_ru;
        $header->title_ua=$request->title_ua;
        $header->text_ru=$request->text_ru;
        $header->text_ua=$request->text_ua;
        $header->description_ru=$request->description_ru;
        $header->description_ua=$request->description_ua;
        $header->address_ru=$request->address_ru;
        $header->address_ua=$request->address_ua;
        $header->phone=$request->phone;
        $header->save();
        $images = $request->file();
        if($images){
            foreach ($images['image'] as $image) {
                $header_image = new HeaderImage;
                $imageName = time().sha1($image).'.'.$image->getClientOriginalExtension();
                $image->move(storage_path('app/public/images/header'), $imageName);
                $header_image->image  = $imageName;
                $header_image->save();
            }
        }
        return redirect('/admin')->with('success', 'Успешно изменено!');
    }
    public function delete_image ($id) {
        $header_image=HeaderImage::find($id)->delete();
        return redirect()->back();
    }
}
