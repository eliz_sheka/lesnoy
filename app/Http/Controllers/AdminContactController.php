<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Validator;

class AdminContactController extends Controller
{
    public function contact() {
        $contact = Contact::first();
//        dd($contact);
        return view('admin.contact_block.contact', compact('contact'));
    }
    public function create_contact(Request $request) {
        $validator = Validator::make($request->all(), [
            'phone' => 'required| max:25',
            'email' => 'required| email',
            'address_ua' => 'required',
            'address_ru' => 'required',
        ])->validate();
        $contact = new Contact;
        $contact->phone = $request->phone;
        $contact->phone2 = $request->phone2;
        $contact->email = $request->email;
        $contact->address_ua = $request->address_ua;
        $contact->address_ru = $request->address_ru;
        $contact->save();

        return back()->with('success', 'Успешно добавлено!');
    }
    public function edit_contact(){
        $contact = Contact::first();
        return view('admin.contact_block.edit_contact', compact('contact'));
    }
    public function update_contact(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'phone' => 'required| max:25',
            'email' => 'required| email',
            'address_ua' => 'required',
            'address_ru' => 'required',
        ])->validate();
//        dd($request);
        $newdata = $request->except('_token');
//        dd($newdata);
//        dd(Header::find($id));
        $update=Contact::find($id)->update($newdata);
//        dd($update);
        return redirect('/admin/contact')->with('success', 'Успешно изменено!');
    }
}
