<?php

namespace App\Http\Controllers;

use App\KidsImage;
use Illuminate\Http\Request;
use App\Kids;

class AdminKidsController extends Controller
{
    public function kids() {
        $kids = Kids::first();
        $kids_image = KidsImage::all();
//        dd($header);
        return view('admin.kids_block.kids', compact('kids','kids_image'));
    }

    public function create_kids(Request $request) {

//        dd($request);
        $data = $request->except('_token');
//        dd($data);
        $kids = new Kids;
        $kids->title_ru=$request->title_ru;
        $kids->title_ua=$request->title_ua;
        $kids->text_ru=$request->text_ru;
        $kids->text_ua=$request->text_ua;
        if ($request->visible) {
            $kids->visible=false;
        }
        $kids->save();
        $images = $request->file();
        if($images){
            foreach ($images['image'] as $image) {
                $gallery_image = new KidsImage;
                $imageName = time().sha1($image).'.'.$image->getClientOriginalExtension();
                $image->move(storage_path('app/public/images/kids'), $imageName);
                $gallery_image->image  = $imageName;
                $gallery_image->save();
            }
        }

        return back()->with('success', 'Успешно добавлено!');
    }
    public function edit_kids() {
        $kids = Kids::first();
        $kids_image = KidsImage::all();
//        dd($kids);
        return view('admin.kids_block.edit_kids', compact('kids','kids_image'));
    }
    public function update_kids(Request $request, $id) {

        $kids = Kids::find($id);
        $kids->title_ru=$request->title_ru;
        $kids->title_ua=$request->title_ua;
        $kids->text_ru=$request->text_ru;
        $kids->text_ua=$request->text_ua;
        if ($request->visible) {
            $kids->visible=false;
        }
        else {
            $kids->visible=true;
        }
        $kids->save();
        $images = $request->file();
        if($images){
            foreach ($images['image'] as $image) {
                $gallery_image = new KidsImage;
                $imageName = time().sha1($image).'.'.$image->getClientOriginalExtension();
                $image->move(storage_path('app/public/images/kids'), $imageName);
                $gallery_image->image  = $imageName;
                $gallery_image->save();
            }
        }
        return redirect('/admin/kids')->with('success', 'Успешно изменено!');
    }

    public function delete_image ($id) {
        $gallery=KidsImage::find($id)->delete();
        return redirect()->back();
    }
}
