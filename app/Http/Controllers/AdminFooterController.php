<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Link;
use App\FooterText;

class AdminFooterController extends Controller
{
    public function footer() {
        $footer = FooterText::first();
        $links = Link::all();
//        dd($link);
        return view('admin.footer_block.footer',compact('footer','links'));
    }
    public function create_footer_text(Request $request) {
        $footer_text = new FooterText;
        $footer_text->text_ua = $request->text_ua;
        $footer_text->text_ru = $request->text_ru;
        $footer_text->save();

        return back()->with('success', 'Успешно добавлено!');
    }
    public function edit_footer_text() {
        $footer = FooterText::first();
        return view('admin.footer_block.edit_footer_text',compact('footer'));
    }
    public function update_footer_text(Request $request, $id) {
        $footer=FooterText::find($id);
        $footer->text_ua = $request->text_ua;
        $footer->text_ru = $request->text_ru;
        $footer->save();
        return redirect('/admin/footer')->with('success', 'Успешно изменено!');
    }

    public function create_link(Request $request) {
        $link = new Link;
        $link->name = $request->name;
        $link->address = $request->address;
        $link->icon = $request->icon;
        $link->save();

        return back()->with('success', 'Успешно добавлено!');
    }
    public function edit_link($id) {
        $link = Link::find($id);
        return view('admin.footer_block.edit_link',compact('link'));
    }
    public function update_link(Request $request, $id) {
        $link=Link::find($id);
        $link->name = $request->name;
        $link->address = $request->address;
        $link->icon = $request->icon;
        $link->save();
        return redirect('/admin/footer')->with('success', 'Успешно изменено!');
    }
    public function delete_link ($id) {
        $link=Link::find($id)->delete();
        return redirect()->back();
    }
}
