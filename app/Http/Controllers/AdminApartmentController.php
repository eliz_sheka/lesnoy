<?php

namespace App\Http\Controllers;

use App\RoomImage;
use Illuminate\Http\Request;
use App\Apartment;
use App\Room;

class AdminApartmentController extends Controller
{
    public function rooms() {
        $apartment = Apartment::first();
        $rooms = Room::all();
        foreach ($rooms as $room) {
            $room->images = $room->room_images;
//            dd($room->images);
        }
//        dd($link);
        return view('admin.apartment_block.apartment',compact('apartment','rooms'));
    }
    public function create_apartment_title(Request $request) {
        $apartment = new Apartment;
        $apartment->title_ua = $request->title_ua;
        $apartment->title_ru = $request->title_ru;
        $apartment->save();

        return back()->with('success', 'Успешно добавлено!');
    }
    public function edit_apartment_title() {
        $apartment = Apartment::first();
        return view('admin.apartment_block.edit_apartment_title',compact('apartment'));
    }
    public function update_apartment_title(Request $request, $id) {
        $apartment=Apartment::find($id);
        $apartment->title_ua = $request->title_ua;
        $apartment->title_ru = $request->title_ru;
        $apartment->save();
        return redirect('/admin/rooms')->with('success', 'Успешно изменено!');
    }

    public function create_rooms(Request $request) {
        $room = new Room;
        $room->name_ua = $request->name_ua;
        $room->name_ru = $request->name_ru;
        $room->info_ua = $request->info_ua;
        $room->info_ru = $request->info_ru;
        $room->save();
        $images = $request->file();
        if($images){
            foreach ($images['image'] as $image) {
                $room_image = new RoomImage;
                $imageName = time().sha1($image).'.'.$image->getClientOriginalExtension();
                $image->move(storage_path('app/public/images/rooms'), $imageName);
                $room_image->image  = $imageName;
                $room_image->room_id=$room->id;
//                dd($room_image);
                $room_image->save();
            }
        }

        return back()->with('success', 'Успешно добавлено!');
    }
    public function edit_room($id) {
        $room = Room::find($id);
        $room->images = $room->room_images;

        return view('admin.apartment_block.edit_room',compact('room'));
    }
    public function update_rooms(Request $request, $id) {
        $room=Room::find($id);
        $room->name_ua = $request->name_ua;
        $room->name_ru = $request->name_ru;
        $room->info_ua = $request->info_ua;
        $room->info_ru = $request->info_ru;
        $room->save();
        $images = $request->file();
        if($images){
            foreach ($images['image'] as $image) {
                $room_image = new RoomImage;
                $imageName = time().sha1($image).'.'.$image->getClientOriginalExtension();
                $image->move(storage_path('app/public/images/rooms'), $imageName);
                $room_image->image  = $imageName;
                $room_image->room_id=$room->id;
                $room_image->save();
            }
        }
        return redirect('/admin/rooms')->with('success', 'Успешно изменено!');
    }
    public function delete_image ($id) {
        $room_image=RoomImage::find($id)->delete();
        return redirect()->back();
    }
    public function delete_room ($id) {
        $room = Room::find($id);
        $room->room_images()->delete();

//        dd($room->room_images());
        $room->delete();
        return redirect()->back();
    }
}
