<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['phone', 'phone2', 'email', 'address_ru', 'address_ua'];
}
