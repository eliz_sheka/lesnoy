<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FooterText extends Model
{
    protected $fillable = ['text_ua', 'text_ru'];
}
