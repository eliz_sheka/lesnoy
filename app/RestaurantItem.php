<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantItem extends Model
{
    protected $fillable = ['title_ua', 'title_ru', 'text_ua', 'text_ru', 'position', 'image'];
}
