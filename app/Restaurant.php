<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $fillable = ['title_ua', 'title_ru', 'menu_ua','menu_ru', 'img_ua', 'img_ru'];
}
