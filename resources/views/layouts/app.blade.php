<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Лесной</title>
    <link rel="stylesheet" href="{{ asset('/assets/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.css')}}" />
    <link rel="shortcut icon" href="/assets/img/favicon.png" type="image/x-icon">
    @yield('head')
</head>
<body>

@yield('content')


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgYFEVo_NUBxN4pU29Z_KaIPA5t9ob9TM">
</script>
<script src="https://use.fontawesome.com/62d1e00140.js"></script>
<script src="{{ asset('/assets/js/app.js') }}"></script>
<script src="{{ asset('/js/custom.js') }}"></script>
</body>
</html>