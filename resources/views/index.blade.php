@extends('layouts.app')
@section('content')
<div class="se-baner"> <!-- se baner start -->
    <div class="container-fluid"> <!-- container-fluid start -->

        <div class="se-baner__slider"> <!-- Baner slider bg -->
            @if(!empty($header_images))
            @foreach ($header_images as $images)
            <div>
                <img class="img-responsive" src="/images/header/{{$images->image}}">
            </div>
<!--            <div>-->
<!--                <img class="img-responsive" src="/assets/img/slide3_big.jpg">-->
<!--            </div>-->
<!--            <div>-->
<!--                <img class="img-responsive" src="/assets/img/slide4_big.jpg">-->
<!--            </div>-->
            @endforeach
            @endif
        </div> <!-- Baner slider bg END -->

        <header class="se-head-mob visible-xs">  <!-- Baner Header mobile start -->
            <div class="container">
                <div class="row clearfix no-gutters">
                    <div class="col-xs-8 text-left">
                        @if($header)
                        <h1 class="se-head-mob__title"><!--«Лесной»-->{{$header->title_ru}}</h1>
                        @endif
                    </div>
                    <div class="col-xs-4 text-right">
                        <a class="se-head-mob__ru" href="/">Рос</a>
                        <a class="se-head-mob__ukr" href="/ua">Укр</a>
                    </div>
                    @if($header)
                    <div class="row no-gutters">
                        <div class="col-xs-12">
                            <strong class="se-head-mob__subtitle"><!--Готельно-ресторанний комплекс-->{{$header->text_ru}}</strong>
                            <p class="se-head-mob__text">{{$header->description_ru}}</p>
<!--                            <p class="se-head-mob__text">Готель «Лесной» пропонує 34 комфортабельних номери різних категорій. Вони підійдуть для зупинки під час подорожі, ділового візиту, відрядження та відпочинку. Кожен номер має всі необхідні зручності та умови комфорту для гарного настрою, релаксу та відпочинку.</p>-->
<!--                            <p class="se-head-mob__text">Доповнять відпочинок кафе та ресторан європейської та української кухні. Для ти, хто подорожує власним авто , у готелі включені послуги парковки під охороною.</p>-->
<!--                            <p class="se-head-mob__text">Готель зручно розташований для відпочинку у дорозі між далекими сполученнями. Ми пропонуємо номери з розміщенням від 1 до 4 осіб з сервісом</p>-->

                            <div class="row no-gutters">
                                <div class="col-xs-2">
                                    <a class="se-head-mob__marker" href="#map-mob"><img  src="assets/img/marker_top.png"></a>
                                </div>
                                <div class="col-xs-10">
                                    <p class="se-head-mob__adres"><!--Харьковская область, Красноградский район, с. Натальино, ул. И. Сенченка, 124-->{{$header->phone}}</p>
                                    <a class="se-head-mob__tel" href="tel:+380506334045"><!--+38 050-633-40-45-->{{$header->phone}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </header> <!-- Baner Header mobile end -->

        <div class="se-banner-content hidden-xs"> <!-- Baner content start -->
            <div class="container">
                <header class="se-header hidden-xs"> <!-- Baner Header start -->
                    <div class="container">
                        <div class="row no-gutter clearfix">
                            @if($header)
                            <div class="col-sm-6 text-left">
                                <div class="se-header__wraper">
                                    <a class="se-header__marker" href="#map">
                                        <img  src="assets/img/marker_top.png">
                                    </a>
                                    <p class="se-header__adres" style="width: 463px;"><!--Харьковская область, Красноградский район, с. Натальино, ул. И. Сенченка, 124-->{{$header->address_ru}}</p>
                                    <a class="se-header__tel" href="tel:+380506334045"><!--+38 050-633-40-45-->{{$header->phone}}</a>

                                    <!-- Button trigger modal -->
                                    <button type="button" class="se-header__info text-left" id="info">Информация <br> о комплексе</button>
                                </div>
                            </div>
                            @endif
                            <div class="col-md-6 text-right">
                                <div class="se-header__lang">
                                    <a class="se-header__ru" href="/">Рос</a>
                                    <a class="se-header__ukr" href="/ua">Укр</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </header> <!-- Baner Header end -->
                <div class="row no-gutters">
                    <div class="col-sm-11 col-sm-offset-1 col-lg-9 col-lg-offset-2">
                        <h1 class="se-header__title"><!--«Лесной»-->{{$header->title_ru}}</h1>
                        <p class="se-header__subtitle"><!--Готельно-ресторанний комплекс-->{{$header->text_ru}}</p>
                        <!-- Modal -->
                        <div class="info-content hidden-xs">
      						<span class="info-close">
      							<img alt="crest" src="assets/img/button_close.png">
      						</span>
                            <h2 class="info-content__title">О комплексе</h2>
                            {{$header->description_ru}}
<!--                            <p class="info-content__text">Готель «Лесной» пропонує 34 комфортабельних номери різних категорій. Вони підійдуть для зупинки під час подорожі, ділового візиту, відрядження та відпочинку. Кожен номер має всі необхідні зручності та умови комфорту для гарного настрою, релаксу та відпочинку.</p>-->
<!--                            <p class="info-content__text">Доповнять відпочинок кафе та ресторан європейської та української кухні. Для ти, хто подорожує власним авто , у готелі включені послуги парковки під охороною.</p>-->
<!--                            <p class="info-content__text">Готель зручно розташований для відпочинку у дорозі між далекими сполученнями. Ми пропонуємо номери з розміщенням від 1 до 4 осіб з сервісом європейських стандартів.</p>-->
                        </div>
                        <a class="se-foto-compl" href="#gallery">Фотографии комплекса</a>
                    </div>
                </div>
            </div>

            <nav class="se-site-nav hidden-xs"> <!-- Baner site nav start -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="se-site-nav__list list-inline" style="text-align: center">
                                <li class="se-site-nav__item">
                                    <a href="#numbers">Номера гостиницы</a>
                                </li>
                                <li class="se-site-nav__item">
                                    <a href="#restoraun">Ресторан</a>
                                </li>
                                <li class="se-site-nav__item">
                                    <a href="#rest">Развлечения и отдых</a>
                                </li>
                                @if($kid->visible == true)
                                <li class="se-site-nav__item">
                                    <a href="#for-kids">Для детей</a>
                                </li>
                                @endif
                                <li class="se-site-nav__item">
                                    <a href="#gallery">Галерея</a>
                                </li>
                                <li class="se-site-nav__item">
                                    <a href="#contact">Контакты</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <a class="se-slide-down" href="#numbers">
                        <img alt="dot" src="assets/img/btn_dot.png">
                    </a>

                </div>
            </nav><!-- Baner site nav start -->

        </div> <!-- Baner content END -->



    </div> <!-- container-fluid end -->
</div> <!-- Baner END -->

<main class="se-main-section">

    <section class="number-mob visible-xs"> <!-- Number mobile start -->
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-xs-12">
                    <h2 class="number-mob__title"><!--34 Затишних номери-->{{$apartment->title_ru}}</h2>
                    <div id="apartments-dropdown-container" class="dropdown visible-xs">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Оберіть номер
                            <span><img src="assets/img/arrow_down.png"></span>
                        </button>
                        <ul id="apartments-dropdowns" class="dropdown-menu dropdown-menu--mob list-unstyled" aria-labelledby="dropdownMenu1">
                            @foreach ($rooms as $key=>$room)
                            <li>
                                <a href="#{{$room->name_ru}}" role="tab" data-toggle="tab">{{$room->name_ru}}</a>
                            </li>
                            @endforeach
<!--                            <li>-->
<!--                                <a href="#halfluxe" role="tab" data-toggle="tab">Напівлюкс</a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="#twoperson" role="tab" data-toggle="tab">Двомісний</a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="#oneperson" role="tab" data-toggle="tab">Одномісний</a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="#fourperson" role="tab" data-toggle="tab">Чотиримісний</a>-->
<!--                            </li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- Number mobile END -->

    <section id="numbers" class="number"> <!-- Number section start -->
        <div class="container number-cont">
            <span class="number__title hidden-xs"><!--34 Затишних номери-->{{$apartment->title_ru}}</span>
            <div class="row no-gutters">
                <div class="col-sm-3 hidden-xs">
                    <ul id="apartments-tab-container" class="nav nav-tabs" role="tablist">
                        @foreach ($rooms as $key=>$room)
                        <li class="{{ $key==0 ? 'active' : ''}}" role="presentation"><a href="#{{$room->name_ru}}" aria-controls="{{$room->name_ru}}" role="tab" data-toggle="tab">{{$room->name_ru}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-9">
                    <div class="tab-content">
                        @foreach($rooms as $key=>$room)
                        <div role="tabpanel" class="tab-pane {{ $key==0 ? 'active' : ''}}" id="{{$room->name_ru}}">


                                @foreach($room->images as $images)
                            <div class="slide">
                                <img class="img-responsive" alt="number" src="/images/rooms/{{$images->image}}">
                            </div>
                                @endforeach

                            <div class="tab-content-mob visible-xs">
                                {!!$room->info_ru!!}
                            </div>

                            <div class="dropup hidden-xs">
                                <button class="btn-up dropdown-toggle" type="button" id="dropdownMenu{{$room->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img alt="elipse" src="assets/img/elipse.png">
                                </button>
                                <div class="dropdown-menu dropdown-menu--1" aria-labelledby="dropdownMenu{{$room->id}}">
                                    {!!$room->info_ru!!}
                                </div>
                                <span class="dropdown-menu__description">Подробнее о номере</span>
                            </div>
                        </div>
                        @endforeach


                        <button class="slide-prev slide-prev--downleft hidden-xs">
                            <img alt="arrow prev" src="assets/img/arrow_left.png">
                        </button>
                        <button  class="slide-next slide-prev--downright hidden-xs">
                            <img alt="arrow next" src="assets/img/arrow_right.png">
                        </button>

                        <div class="slide-counter hidden-xs visible-md visible-lg">
                            <span class="current-position">1</span> / <span class="amount">1</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- Number section End -->

    <section id="restoraun" class="se-kitchen"> <!-- Kitchen section start -->
        <div class="container">
            <div class="row no-gutters">

                <h2 class="se-kitchen__title"><!--Ресторан європейської та української кухні-->{{$restaurant->title_ru}}</h2>
                <h5 class="se-kitchen__subtitle visible-xs"><!--Меню ресторана-->{{$restaurant_item1->title_ru}}</h5>
                <div class="col-xs-9 col-sm-6">
                    <div class="se-image-wraper se-image-wraper--left">

                        <img class="img-responsive" alt="borsh" src="images/restaurant_item/{{$restaurant_item1->image}}">

                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-6 padding_left">

                    <h5 class="se-kitchen__subtitle hidden-xs"><!--Меню ресторана-->{{$restaurant_item1->title_ru}}</h5>
                    <p class="se-kitchen__content">{!!$restaurant_item1->text_ru!!}</p>
                    @if($restaurant->menu_ru)
                    <div class="se-menu">
                        <a href="/restaurant/menu/{{$restaurant->menu_ru}}"><img height="45" alt="pdf" src="restaurant/menu/{{$restaurant->img_ru}}"> Посмотреть меню</a>
                        <span>{{$restaurant->size_ru > 1000 ? ($report->size/1000).' мб' : $restaurant->size_ru.' кб' }}</span>
                    </div>
                    @endif
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-xs-12 col-sm-6 col-md-6 padding_left">

                    <h5 class="se-kitchen__subtitle se-kitchen__subtitle2">{{$restaurant_item2->title_ru}}</h5>
<!--                    <ul class="se-kitchen-list list-unstyled hidden-xs">-->
<!--                        <li class="se-kitchen-list__item">Проведення банкетів, фуршетів</li>-->
<!--                        <li class="se-kitchen-list__item">Організацію свят</li>-->
<!--                        <li class="se-kitchen-list__item">Оренду залу для проведення семінарів, форумів,  робочих зустрічей.</li>-->
<!--                    </ul>-->
                    {!!$restaurant_item2->text_ru!!}

                </div>

                <div class="col-xs-9 col-sm-6 col-md-6">
                    <div class="se-image-wraper se-image-wraper--right">

                        <img class="img-responsive" alt="bar" src="images/restaurant_item/{{$restaurant_item2->image}}">

                    </div>
                </div>

                <div class="row no-gutters visible-xs hidden-sm">
                    <div class="col-xs-12 padding_left">
                        {!!$restaurant_item2->text_ru!!}
<!--                        <ul class="se-kitchen-list list-unstyled">-->
<!--                            <li class="se-kitchen-list__item">Проведення банкетів, фуршетів</li>-->
<!--                            <li class="se-kitchen-list__item">Організацію свят</li>-->
<!--                            <li class="se-kitchen-list__item">Оренду залу для проведення семінарів, форумів, робочих зустрічей.</li>-->
<!--                        </ul>-->

                    </div>
                </div>

            </div>
            <div class="row no-gutters">
                <div class="col-xs-10 col-sm-6">
                    <h5 class="se-kitchen__subtitle se-kitchen__subtitle--third visible-xs">{!!$restaurant_item3->title_ru!!}</h5>

                    <div class="se-image-wraper se-image-wraper--third">
                        <img class="img-responsive" alt="kitchen" src="images/restaurant_item/{{$restaurant_item3->image}}">
                    </div>

                </div>
                <div class="col-xs-12 col-sm-6 padding_left">
                    <h5 class="se-kitchen__subtitle hidden-xs">{!!$restaurant_item3->title_ru!!}</h5>
                    <p class="se-kitchen__content">{!!$restaurant_item3->text_ru!!}</p>
                </div>
            </div>
        </div>
    </section> <!-- Kitchen dection END -->

    <section id="rest" class="se-entertainment"> <!-- Entertainment section start -->
        <div class="container">
            <h2 class="se-entertainment__title">{{$pastime->title_ru}}</h2>
            <p class="se-entertainment__text">{{$pastime->text_ru}}</p>
<!--            <p class="se-entertainment__text">Більярдна допоможе весело провести час. Більярдний стіл, відточений кий, матові кулі та м’яке світло зроблять вечір особливим.</p>-->
            <div class="row no-gutters">

                <div class="col-sm-12">
                    <div class="se-slider-box hidden-xs">
                        <div id="carousel">
                            @foreach($pastime_images as $images)
                            <a href="#"><img class="img-responsive"  data-description="{{$images->title_ru}}" alt="rest" src="images/pastime/{{$images->image}}" id="item-{{$images->id}}" /></a>
                            @endforeach
<!--                            <a href="#"><img class="img-responsive" alt="rest" src="assets/img/rest_slide2.jpg" id="item-2" /></a>-->
<!--                            <a href="#"><img class="img-responsive" alt="rest" src="assets/img/rest_slide2.jpg" id="item-3" /></a>-->
                        </div>
                        <button class="slide-prev slide-prev--leftbtn">
                            <img  alt="arrow prev" src="assets/img/arrow_left.png">
                        </button>
                        <button  class="slide-next slide-next--rightbtn">
                            <img alt="arrow next" src="assets/img/arrow_right.png">
                        </button>
                        <div class="se-rectangle-bg"></div>
                    </div>
                </div>

                <div class="se-entertainment-mob visible-xs">
                    <div class="row no-gutters">
                        <div class="col-xs-12">
                            @foreach($pastime_images as $images)
                            <img class="img-responsive"  src="images/pastime/{{$images->image}}">
                            <strong class="se-entertainment-mob__imgdescr">{{$images->title_ru}}</strong>
                            @endforeach
<!--                            <img class="img-responsive" src="assets/img/rest_slide2.jpg">-->
<!--                            <strong class="se-entertainment-mob__imgdescr">Трекинг по лесному массиву</strong>-->
<!--                            <img class="img-responsive" src="assets/img/rest_slide1.jpg">-->
<!--                            <strong class="se-entertainment-mob__imgdescr">Трекинг по лесному массиву</strong>-->
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <h4 class="se-entertainment__subtitle hidden-xs gallery_description" style="text-align: center"></h4>

    </section> <!-- Entertainment section start -->
@if($kid->visible == true)
    <section id="for-kids" class="se-kids"> <!-- For Kids section start -->
        <div class="container"> <!-- Container start -->
            <div class="row no-gutters"> <!-- Row start -->

                <div class="col-xs-12 col-sm-7">

                    <h2 class="se-kids__title visible-xs">{{$kid->title_ru}}</h2>
                    <div id="kids" class="kids-slider-wraper">
                        <div  class="kids-slider">
                            @foreach ($kid_images as $images)
                            <div class="kids-slide">
                                <img class="img-responsive" alt="slide1" src="images/kids/{{$images->image}}">
                            </div>
                            @endforeach
<!--                            <div class="kids-slide">-->
<!--                                <img class="img-responsive" alt="slide1" src="assets/img/kids_slide1.jpg">-->
<!--                            </div>-->
<!--                            <div class="kids-slide">-->
<!--                                <img class="img-responsive" alt="slide1" src="assets/img/kids_slide1.jpg">-->
<!--                            </div>-->
<!--                            <div class="kids-slide">-->
<!--                                <img class="img-responsive" alt="slide1" src="assets/img/kids_slide1.jpg">-->
<!--                            </div>-->
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-5">
                    <div class="se-wraper">
                        <h2 class="se-kids__title hidden-xs">{{$kid->title_ru}}</h2>
                        <p class="se-kids__subtitle">{{$kid->text_ru}}</p>
<!--                        <p class="se-kids__text">У Вашому розпорядженні є прекрасні зони для відпочинку дітей та дитячі майданчики. Розміщення у номерах готелю сімей з дітьми також дуже комфортне. В повній мірі можна сказати, що ми створили справжній готель для відпочинку з дітьми.</p>-->
                        <button class="slide-prev slide-prev--kidsprev hidden-xs">
                            <img  alt="arrow prev" src="assets/img/arrow_left.png">
                        </button>
                        <button class="slide-next slide-next--kidsnext hidden-xs">
                            <img alt="arrow next" src="assets/img/arrow_right.png">
                        </button>
                    </div>
                </div>

            </div> <!-- Row End -->
        </div> <!-- Container end -->
    </section> <!-- For Kids section End -->
@endif
    <section id="gallery" class="se-gallery"> <!-- Gallery section End -->
        <div class="container">
            <div class="row no-gutters">
                <div class="col-xs-12 col-md-12">
                    <h2 class="se-gallery__title text-center">{{$gallery->title_ru}}</h2>

                    <div id="gallery-slider-mob" class="gallery-slider-wraper visible-xs">

                        <div  class="gallery-slider">
                            @foreach ($gallery_images as $images)
                            <div class="gallery-slide">
                                <img alt="slide1" class="img-responsive" src="images/gallery/{{$images->image}}">
                            </div>
                            @endforeach
<!--                            <div class="gallery-slide">-->
<!--                                <img alt="slide1" class="img-responsive" src="assets/img/kids_slide1.jpg">-->
<!--                            </div>-->
<!--                            <div class="gallery-slide">-->
<!--                                <img alt="slide1" class="img-responsive" src="assets/img/kids_slide1.jpg">-->
<!--                            </div>-->
<!--                            <div class="gallery-slide">-->
<!--                                <img alt="slide1" class="img-responsive" src="assets/img/kids_slide1.jpg">-->
<!--                            </div>-->
                        </div>

                        <button class="slide-prev slide-prev--sprev visible-xs">
                            <img  alt="arrow prev" src="assets/img/arrow_left.png">
                        </button>
                        <button class="slide-next slide-next--snext visible-xs">
                            <img alt="arrow next" src="assets/img/arrow_right.png">
                        </button>
                    </div>

                    <div class="se-flex-box hidden-xs">
                        @foreach ($gallery_images as $key =>$images)
                        <div class="se-flex-block se-flex-block--block{{$key+1}}">
                            <img alt="slide"  src="images/gallery/{{$images->image}}">
                        </div>
                        @endforeach
<!--                        <div class="se-flex-block se-flex-block--block2">-->
<!--                            <img alt="slide" class="img-responsive" src="assets/img/gallery-img2.jpg">-->
<!--                        </div>-->
<!--                        <div class="se-flex-block se-flex-block--block3">-->
<!--                            <img alt="slide" class="img-responsive" src="assets/img/gallery-img3.jpg">-->
<!--                        </div>-->
<!--                        <div class="se-flex-block se-flex-block--block4">-->
<!--                            <img alt="slide" class="img-responsive" src="assets/img/gallery-img4.jpg">-->
<!--                        </div>-->
<!--                        <div class="se-flex-block se-flex-block--block5">-->
<!--                            <img alt="slide" class="img-responsive" src="assets/img/gallery-img5.jpg">-->
<!--                        </div>-->
<!--                        <div class="se-flex-block se-flex-block--block6">-->
<!--                            <img class="img-responsive" src="assets/img/gallery-img6.jpg">-->
<!--                        </div>-->
                    </div>

                </div>
            </div>
        </div>
    </section> <!-- Gallery section End -->

    <footer id="contact" class="se-footer"> <!-- Footer section Start -->
        <div class="container-fluid contacts">
            <div class="row no-gutters">

                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-sm-6">
                            <div class="se-contact clearfix">
                                <h2 class="se-contact__title">Контактная информация</h2>
                                <h4 class="se-contact__subtitle">Телефоны</h4>
                                <a class="se-contact__tel" href="tel:+380501233333">{{$contact->phone}}</a>
                                <br>
                                @if(!empty($contact->phone2))
                                <a class="se-contact__tel" href="tel:+380501233333">{{$contact->phone2}}</a>
                                @endif
                                <h4 class="se-contact__subtitle se-contact__subtitle--mail">E-Mail</h4>
                                <a class="se-contact__email" href="mailto:booking@lesnoy.com.ua">	{{$contact->email}}</a>
                                <h4 class="se-contact__subtitle se-contact__subtitle--adr">Адрес</h4>
                                <p class="se-contact__addres">{{$contact->address_ru}}</p>

                                <div class="flex-box-social"> <!-- Social flex box -->
                                    <div class="copyright-box clearfix">
                                        <p class="se-contact__copyright">{{$footer->text_ru}}</p>
                                    </div>

                                    <div class="social-box text-right clearfix">
                                        <ul class="social-mob list-inline">
                                            @foreach ($links as $link)
                                            <li>
                                                <a class="fa {{$link->icon}}" href="{{$link->address}}"></a>
                                            </li>
                                            @endforeach
                                            <!-- <li>
                                                <a class="fa fa-twitter" href="#"></a>
                                            </li> -->
<!--                                            <li>-->
<!--                                                <a class="fa fa-facebook" href="#"></a>-->
<!--                                            </li>-->
                                            <!-- <li>
                                                <a class="fa fa-instagram" href="#"></a>
                                            </li> -->
                                            <!-- <li>
                                                <a class="fa fa-google-plus" href="#"></a>
                                            </li> -->
                                            <!-- <li>
                                                <a class="fa fa-vk" href="#"></a>
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>  <!-- Social flex box END -->

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 float-map">
                    <div id="map" class="se-map">
<!--                There are an error "InvalidValueError: setPosition: not a LatLng or LatLngLiteral:
                    in property lng: not a number", so parameters were changed in app.js (11587-11604)-->
                        <div class="se-marker" data-lat="49.335711" data-lng="35.478930"></div>
                    </div>
                </div>
            </div>
        </div>
    </footer> <!-- Footer section END -->
</main>
@endsection