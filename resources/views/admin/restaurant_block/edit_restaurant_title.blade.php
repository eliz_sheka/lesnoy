@extends('admin.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование</h1>
    </div>
    <div class="row header_table col-sm-10">
        <form method="post" action="/admin/update_restaurant_title/{{$restaurant->id}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title_ru">Заголовок (русский)</label>
                <input required value="{{ $restaurant->title_ru }}" type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Заголовок (русский)">
                @if ($errors->has('title_ru'))
                <span class="help-block">
                                            <strong>{{ $errors->first('title_ru') }}</strong>
                                        </span>
                @endif
            </div>
            <div class="form-group">
                <label for="title_ua">Заголовок (українська)</label>
                <input required value="{{$restaurant->title_ua }}" type="text" class="form-control" name="title_ua" id="title_ua" placeholder="Заголовок (українська)">
                @if ($errors->has('title_ua'))
                <span class="help-block">
                                            <strong>{{ $errors->first('title_ua') }}</strong>
                                        </span>
                @endif
            </div>
            <ul class="menu_list">
                <li class="col-sm-6">
                @if(!$restaurant->menu_ua)
                    <div class="form-group">
                        <label for="menu_ua">Меню (українська)</label>
                        <input value="{{ old('menu_ua') }}" type="file" class="form-control" name="menu_ua" id="menu_ua" placeholder="Меню (українська)">
                        @if ($errors->has('menu_ua'))
                        <span class="help-block">
                                            <strong>{{ $errors->first('menu_ua') }}</strong>
                                        </span>
                        @endif
                    </div>
                @endif
                @if($restaurant->menu_ua)
                    <div class="img_block">
                        <a href="/restaurant/menu/{{$restaurant->menu_ua}}" target="_blank"><img title="Просмотреть" width="40" src='/restaurant/menu/{{$restaurant->img_ua}}'></a>
                        <p>Меню <br>(украинский)</p>
                        <a href='/admin/delete_menu_ua/{{$restaurant->id}}' type="button"  class="btn btn-default btn-xs delete_btn" ><i class="fa fa-times" aria-hidden="true"></i></a>
                    </div>
                @endif
                </li>
                <li class="col-sm-6">
                @if(!$restaurant->menu_ru)
                    <div class="form-group col-sm-8">
                        <label for="menu_ru">Меню (русский)</label>
                        <input value="{{ old('menu_ru') }}" type="file" class="form-control" name="menu_ru" id="menu_ru" placeholder="Меню (русский)">
                        @if ($errors->has('menu_ru'))
                        <span class="help-block">
                                            <strong>{{ $errors->first('menu_ru') }}</strong>
                                        </span>
                        @endif
                    </div>
                @endif
                    @if($restaurant->menu_ru)
                    <div class="img_block">
                        <a href="/restaurant/menu/{{$restaurant->menu_ru}}" target="_blank"><img title="Просмотреть" width="40" src='/restaurant/menu/{{$restaurant->img_ru}}'></a>
                        <p>Меню <br>(русский)</p>
                        <a href='/admin/delete_menu_ru/{{$restaurant->id}}' type="button" class="btn btn-default btn-xs delete_btn" ><i class="fa fa-times" aria-hidden="true"></i></a>
                    </div>

                @endif
                </li>
            </ul>
            <button type="submit" class="btn btn-default pull-right">Сохранить</button>
        </form>
    </div>
</div>
@endsection