@extends('admin.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование</h1>
    </div>
    <div class="row header_table col-sm-10">
        <form method="post" action="/admin/update_restaurant_item/{{$restaurant_item->id}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title_ua">Назва (українська)</label>
                <input required value="{{ $restaurant_item->title_ua }}" type="text" class="form-control" name="title_ua" id="title_ua" placeholder="Название (українська)">
                @if ($errors->has('title_ua'))
                <span class="help-block">
                                <strong>{{ $errors->first('title_ua') }}</strong>
                            </span>
                @endif
            </div>
            <div class="form-group">
                <label for="title_ru">Название (русский)</label>
                <input required value="{{ $restaurant_item->title_ru }}" type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Название (русский)">
                @if ($errors->has('title_ru'))
                <span class="help-block">
                                <strong>{{ $errors->first('title_ru') }}</strong>
                            </span>
                @endif
            </div>
            <div class="form-group">
                <label for="text_ua">Текст (українська)</label>
                <textarea required rows="5" class="form-control" name="text_ua" id="text_ua" placeholder="Текст (українська)">{{$restaurant_item->text_ua}}</textarea>
                @if ($errors->has('text_ua'))
                <span class="help-block">
                                        <strong>{{ $errors->first('text_ua') }}</strong>
                                    </span>
                @endif
            </div>
            <script>
                CKEDITOR.replace( 'text_ua' );
            </script>
            <div class="form-group">
                <label for="text_ru">Текст (русский)</label>
                <textarea required rows="5" class="form-control" name="text_ru" id="text_ru" placeholder="Текст (русский)">{{$restaurant_item->text_ru}}</textarea>
                @if ($errors->has('text_ru'))
                <span class="help-block">
                            <strong>{{ $errors->first('text_ru') }}</strong>
                        </span>
                @endif
            </div>
            <script>
                CKEDITOR.replace( 'text_ru' );
            </script>
            <div class="form-group">
                <label for="position">Позиция</label>
                <select  rows="5" class="form-control" name="position" id="position" placeholder="Текст (русский)">
                    <option value="1" @if(($restaurant_item->position)==1) selected @endif>1</option>
                    <option value="2" @if(($restaurant_item->position)==2) selected @endif>2</option>
                    <option value="3" @if(($restaurant_item->position)==3) selected @endif>3</option>
                    <option value="" @if(($restaurant_item->position)=="") selected @endif >Нет</option>
                </select>
                @if ($errors->has('position'))
                <span class="help-block">
                            <strong>{{ $errors->first('position') }}</strong>
                        </span>
                @endif
            </div>
            @if($restaurant_item->image)
            <div class="img_block">
                <img width="130" src='/images/restaurant_item/{{$restaurant_item->image}}'>
                <a href='/admin/delete_image/{{$restaurant_item->id}}' type="button" class="btn btn-default btn-xs delete_btn" ><i class="fa fa-times" aria-hidden="true"></i></a>
            </div>
            @endif
            @if(!$restaurant_item->image)
            <div class="form-group">
                <label for="image">Выбрать изображение</label>
                <input type="file" class="form-control" name="image" id="image">
                @if ($errors->has('image'))
                <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
                @endif
            </div>
            @endif
            <button type="submit" class="btn btn-default pull-right">Сохранить</button>
        </form>
    </div>
</div>
@endsection