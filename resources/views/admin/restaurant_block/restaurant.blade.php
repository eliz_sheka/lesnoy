@extends('admin.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Ресторан <small style="font-size: 14px;" class="text-success">{{ session('success') }}</small></h1>
    </div>
    <div class="col-md-11 col-xs-12">
        @if(empty($restaurant))
        <button data-toggle="collapse" data-target="#demo" class="btn btn-default add_btn" style="margin-bottom: 5px">Добавить заголовок</button>
        @endif
        @if($restaurant)
        <a type="button" href="/admin/edit_restaurant_title" class="btn btn-default add_btn">Редактировать</a>
        @endif
        <div class="row collapse add_header" id="demo">
            <div class="col-sm-12">
                <form method="post" action="/admin/create_restaurant_title" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="title_ru">Заголовок (русский)</label>
                        <input required value="{{ old('title_ru') }}" type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Заголовок (русский)">
                        @if ($errors->has('title_ru'))
                        <span class="help-block">
                                            <strong>{{ $errors->first('title_ru') }}</strong>
                                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="title_ua">Заголовок (українська)</label>
                        <input required value="{{ old('title_ua') }}" type="text" class="form-control" name="title_ua" id="title_ua" placeholder="Заголовок (українська)">
                        @if ($errors->has('title_ua'))
                        <span class="help-block">
                                            <strong>{{ $errors->first('title_ua') }}</strong>
                                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="menu_ua">Меню (українська)</label>
                        <input value="{{ old('menu_ua') }}" type="file" class="form-control" name="menu_ua" id="menu_ua" placeholder="Меню (українська)">
                        @if ($errors->has('menu_ua'))
                        <span class="help-block">
                                            <strong>{{ $errors->first('menu_ua') }}</strong>
                                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="menu_ru">Меню (русский)</label>
                        <input value="{{ old('menu_ru') }}" type="file" class="form-control" name="menu_ru" id="menu_ru" placeholder="Меню (русский)">
                        @if ($errors->has('menu_ru'))
                        <span class="help-block">
                                            <strong>{{ $errors->first('menu_ru') }}</strong>
                                        </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-default pull-right">Сохранить</button>
                </form>
            </div>
        </div>
        @if(!empty($restaurant))
        <div class="header_table col-sm-12">
            <table class="table ">
                <thead>
                    <tr>
                        <th colspan="2">Українська</th>
                        <th colspan="2">Русский</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-sm-4">{{$restaurant->title_ua}}</td>
                        <td style="border-right: 1px solid #ddd;" class="col-sm-2"><a href="/restaurant/menu/{{$restaurant->menu_ua}}" target="_blank">{{$restaurant->menu_ua}}</a></td>
                        <td class="col-sm-4">{{$restaurant->title_ru}}</td>
                        <td class="col-sm-2"><a href="/restaurant/menu/{{$restaurant->menu_ru}}" target="_blank">{{$restaurant->menu_ru}}</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        @endif
    </div>
    <div class="col-md-11 col-xs-12">
        <button data-toggle="collapse" data-target="#demo1" class="btn btn-default add_btn">Добавить блок</button>
        <div class="row collapse add_header" id="demo1">
            <div class="col-sm-12">
                <form method="post" action="/admin/create_restaurant_item" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="title_ua">Назва (українська)</label>
                        <input required value="{{ old('title_ua') }}" type="text" class="form-control" name="title_ua" id="title_ua" placeholder="Название (українська)">
                        @if ($errors->has('title_ua'))
                        <span class="help-block">
                                <strong>{{ $errors->first('title_ua') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="title_ru">Название (русский)</label>
                        <input required value="{{ old('title_ru') }}" type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Название (русский)">
                        @if ($errors->has('title_ru'))
                        <span class="help-block">
                                <strong>{{ $errors->first('title_ru') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="text_ua">Текст (українська)</label>
                        <textarea required rows="5" class="form-control" name="text_ua" id="text_ua" placeholder="Текст (українська)"></textarea>
                        @if ($errors->has('text_ua'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('text_ua') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <script>
                        CKEDITOR.replace( 'text_ua' );
                    </script>
                    <div class="form-group">
                        <label for="text_ru">Текст (русский)</label>
                        <textarea required rows="5" class="form-control" name="text_ru" id="text_ru" placeholder="Текст (русский)"></textarea>
                        @if ($errors->has('text_ru'))
                        <span class="help-block">
                            <strong>{{ $errors->first('text_ru') }}</strong>
                        </span>
                        @endif
                    </div>
                    <script>
                        CKEDITOR.replace( 'text_ru' );
                    </script>
                    <div class="form-group">
                        <label for="position">Позиция</label>
                        <select  rows="5" class="form-control" name="position" id="position" placeholder="Текст (русский)">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option selected value="">Нет</option>
                        </select>
                        @if ($errors->has('position'))
                        <span class="help-block">
                            <strong>{{ $errors->first('position') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="image">Выбрать изображение</label>
                        <input type="file" class="form-control" name="image" id="image">
                    </div>
                    <button type="submit" class="btn btn-default pull-right">Сохранить</button>
                </form>
            </div>
        </div>
        @if(!empty($restaurant_items))
        <div class="row header_table">
            <table class="table ">
                <thead>
                <tr>
                    <th class="col-sm-4">Українська</th>
                    <th class="col-sm-4">Русский</th>
                    <th>Позиция</th>
                </tr>
                </thead>
                <tbody>
                @foreach($restaurant_items as $item)
                <tr>
                    <td class="col-sm-5" style="border-right: 1px solid #ddd;">
                        <b>{{$item->title_ua}}</b><br>
                        {!!$item->text_ua!!}
                    </td>
                    <td class="col-sm-5" style="border-right: 1px solid #ddd;">
                        <b>{{$item->title_ru}}</b><br>
                        {!!$item->text_ru!!}
                    </td>
                    <td >{{$item->position}}</td>
                    <td><a href="/admin/edit_restaurant_item/{{$item->id}}"><i class="fa fa-pencil"></i></a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @endif
    </div>
</div>

@endsection