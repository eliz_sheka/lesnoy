@extends('admin.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Футер <small style="font-size: 14px;" class="text-success">{{ session('success') }}</small></h1>
    </div>
        <div class="col-md-11 col-xs-12">
            @if(empty($footer))
            <button style="margin-bottom: 5px;" data-toggle="collapse" data-target="#demo" class="btn btn-default add_btn">Добавить текст</button>
            @endif
            @if($footer)
            <a  type="button" href="/admin/edit_footer_text" class="btn btn-default add_btn">Редактировать текст</a>
            @endif
            <div class="row collapse add_header" id="demo">
                <div class="col-sm-12">
                    <form method="post" action="/admin/create_footer_text">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="text_ru">Текст (русский)</label>
                            <input required value="{{ old('text_ru') }}" type="text" class="form-control" name="text_ru" id="text_ru" placeholder="Текст (русский)">
                            @if ($errors->has('text_ru'))
                            <span class="help-block">
                                            <strong>{{ $errors->first('text_ru') }}</strong>
                                        </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="text_ua">Текст (українська)</label>
                            <input required value="{{ old('text_ua') }}" type="text" class="form-control" name="text_ua" id="text_ua" placeholder="Текст (українська)">
                            @if ($errors->has('text_ua'))
                            <span class="help-block">
                                            <strong>{{ $errors->first('text_ua') }}</strong>
                                        </span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-default pull-right">Сохранить</button>
                    </form>
                </div>
            </div>
        @if(!empty($footer))
        <div class="header_table col-sm-12">
            <table class="table ">
                <thead>
                    <tr>
                        <th class="col-sm-6">Українська</th>
                        <th class="col-sm-6">Русский</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-sm-6" style="border-right: 1px solid #ddd;">{{$footer->text_ua}}</td>
                        <td class="col-sm-6">{{$footer->text_ru}}</td>
                    </tr>

                </tbody>
            </table>
        </div>
        @endif
    </div>
    <div class="col-md-11 col-xs-12">
        <button data-toggle="collapse" data-target="#demo1" class="btn btn-default add_btn">Добавить ссылку</button>
        <div class="row collapse add_header" id="demo1">
            <div class="col-sm-12">
                <form method="post" action="/admin/create_link">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Название</label>
                        <input required value="{{ old('name') }}" type="text" class="form-control" name="name" id="name" placeholder="Название">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="address">Ссылка</label>
                        <input required value="{{ old('address') }}" type="text" class="form-control" name="address" id="address" placeholder="Ссылка">
                        @if ($errors->has('address'))
                        <span class="help-block">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="icon">Иконка (fa-facebook)</label>
                        <input required value="{{ old('icon') }}" type="text" class="form-control" name="icon" id="icon" placeholder="Иконка">
                        @if ($errors->has('icon'))
                        <span class="help-block">
                            <strong>{{ $errors->first('icon') }}</strong>
                        </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-default pull-right">Сохранить</button>
                </form>
            </div>
        </div>
        @if(!empty($links))
        <div class="row header_table">
            <div class=" col-sm-6">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Иконка</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($links as $link)
                        <tr>
                            <td>{{$link->name}}</td>
                            <td class="social-mob"><i class="fa {{$link->icon}}" aria-hidden="true"></i></td>
                            <td><a href="/admin/edit_link/{{$link->id}}"><i class="fa fa-pencil"></i></a></td>
                            <td><a href="/admin/delete_link/{{$link->id}}"><i class="fa fa-times"></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
    </div>
</div>




@endsection