@extends('admin.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование</h1>
    </div>
    <div class="row header_table col-sm-10">
        <form method="post" action="/admin/update_link/{{$link->id}}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Название</label>
                <input required value="{{ $link->name }}" type="text" class="form-control" name="name" id="name" placeholder="Название">
                @if ($errors->has('name'))
                <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                @endif
            </div>
            <div class="form-group">
                <label for="address">Ссылка</label>
                <input required value="{{ $link->address }}" type="text" class="form-control" name="address" id="address" placeholder="Ссылка">
                @if ($errors->has('address'))
                <span class="help-block">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                @endif
            </div>
            <div class="form-group">
                <label for="icon">Иконка</label>
                <input required value="{{ $link->icon }}" type="text" class="form-control" name="icon" id="icon" placeholder="Иконка">
                @if ($errors->has('icon'))
                <span class="help-block">
                            <strong>{{ $errors->first('icon') }}</strong>
                        </span>
                @endif
            </div>
            <button type="submit" class="btn btn-default pull-right">Сохранить</button>
        </form>
    </div>
</div>
@endsection