@extends('admin.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование</h1>
    </div>
    <div class="row header_table col-sm-10">
        <form method="post" action="/admin/update_footer_text/{{$footer->id}}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="text_ru">Текст (русский)</label>
                <input required value="{{ $footer->text_ru }}" type="text" class="form-control" name="text_ru" id="text_ru" placeholder="Текст (русский)">
                @if ($errors->has('text_ru'))
                <span class="help-block">
                    <strong>{{ $errors->first('text_ru') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="text_ua">Текст (українська)</label>
                <input required value="{{ $footer->text_ua }}" type="text" class="form-control" name="text_ua" id="text_ua" placeholder="Текст (українська)">
                @if ($errors->has('text_ua'))
                <span class="help-block">
                    <strong>{{ $errors->first('text_ua') }}</strong>
                </span>
                @endif
            </div>
            <button type="submit" class="btn btn-default pull-right">Сохранить</button>
        </form>
    </div>
</div>
@endsection