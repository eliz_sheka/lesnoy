@extends('admin.app')
@section('content')
<div class="row">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Главная <small style="font-size: 14px;" class="text-success">{{ session('success') }}</small></h1>
            </div>

            @if(empty($header))
            <button data-toggle="collapse" data-target="#demo" class="btn btn-default add_btn">Добавить</button>
            @endif
            @if($header)
            <a type="button" href="/admin/edit_header" class="btn btn-default add_btn">Редактировать текст</a>
            @endif
            <div class="row collapse add_header" id="demo">
                <div class="col-sm-8">
                        <form method="post" action="/admin/create_header" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="title_ru">Заголовок (русский)</label>
                                <input required value="{{ old('title_ru') }}" type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Заголовок (русский)">
                                @if ($errors->has('title_ru'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('title_ru') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="title_ua">Заголовок (українська)</label>
                                <input required value="{{ old('title_ua') }}" type="text" class="form-control" name="title_ua" id="title_ua" placeholder="Заголовок (українська)">
                                @if ($errors->has('title_ua'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('title_ua') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="text_ru">Текст (русский)</label>
                                <textarea required rows="5" class="form-control" name="text_ru" id="text_ru" placeholder="Текст (русский)"></textarea>
                                @if ($errors->has('text_ru'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('text_ru') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="text_ua">Текст (українська)</label>
                                <textarea required rows="5" class="form-control" name="text_ua" id="text_ua" placeholder="Текст (українська)"></textarea>
                                @if ($errors->has('text_ua'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('text_ua') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="description_ru">О комплексе (русский)</label>
                                <textarea required rows="5" class="form-control" name="description_ru" id="description_ru" placeholder="О комплексе (русский)"></textarea>
                                @if ($errors->has('description_ru'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('description_ru') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="description_ua">Про комплекс (українська)</label>
                                <textarea required rows="5" class="form-control" name="description_ua" id="description_ua" placeholder="Про комплекс (українська)"></textarea>
                                @if ($errors->has('description_ua'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('description_ua') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="address_ru">Адресс (русский)</label>
                                <input required value="{{ old('address_ru') }}" class="form-control" name="address_ru" id="address_ru" placeholder="Адресс (русский)">
                                @if ($errors->has('address_ru'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('address_ru') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="address_ua">Адресa (українська)</label>
                                <input required value="{{ old('address_ua') }}" class="form-control" name="address_ua" id="address_ua" placeholder="Адресa (українська)">
                                @if ($errors->has('address_ua'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_ua') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="phone">Номер телефона</label>
                                <input required value="{{ old('phone') }}" type="text" class="form-control" name="phone" id="phone" placeholder="Номер телефона">
                                @if ($errors->has('phone'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="image">Выбрать изображения</label>
                                <input type="file" class="form-control" name="image[]" id="image" multiple>
                                @if ($errors->has('image'))
                                <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-default pull-right">Сохранить</button>
                        </form>
                </div>
            </div>
        </div>

@if(!empty($header))
        <div class="row header_table col-sm-10">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#ukrainian" aria-controls="ukrainian" role="tab" data-toggle="tab">Українська</a></li>
                <li role="presentation"><a href="#russian" aria-controls="russian" role="tab" data-toggle="tab">Русский</a></li>
            </ul>
            <div class="tab-content">
                <div id="ukrainian" role="tabpanel" class="tab-pane active ">
                    <table class="table ">
                        <tbody>
                            <tr>
                                <th>Заголовок</th>
                                <td>{{$header->title_ua}}</td>
                            </tr>
                            <tr>
                                <th>Текст</th>
                                <td>{{$header->text_ua}}</td>
                            </tr>
                            <tr>
                                <th>Опис для popup</th>
                                <td>{{$header->description_ua}}</td>
                            </tr>
                            <tr>
                                <th>Адреса</th>
                                <td>{{$header->address_ua}}</td>
                            </tr>
                            <tr>
                                <th>Номер</th>
                                <td>{{$header->phone}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="russian" role="tabpanel" class="tab-pane" >
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>Заголовок</th>
                                <td>{{$header->title_ru}}</td>
                            </tr>
                            <tr>
                                <th>Текст</th>
                                <td>{{$header->text_ru}}</td>
                            </tr>
                            <tr>
                                <th>Описание для popup</th>
                                <td>{{$header->description_ru}}</td>
                            </tr>
                            <tr>
                                <th>Адресс</th>
                                <td>{{$header->address_ru}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            @if($header_images)
            @foreach($header_images as $image)
            <div class="col-sm-4" style="margin-bottom: 10px">
                <img width="200" src="/images/header/{{$image->image}}">
            </div>
            @endforeach
            @endif
        </div>
@endif
</div>
@endsection