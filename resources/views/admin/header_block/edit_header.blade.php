@extends('admin.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование</h1>
    </div>
    <div class="row header_table col-sm-10">
            <form method="post" action="/admin/update_header/{{$header->id}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="title_ru">Заголовок (русский)</label>
                    <input type="text" class="form-control" name="title_ru" id="title_ru" value="{{$header->title_ru}}">
                </div>
                <div class="form-group">
                    <label for="title_ua">Заголовок (українська)</label>
                    <input type="text" class="form-control" name="title_ua" id="title_ua"  value="{{$header->title_ua}}">
                </div>
                <div class="form-group">
                    <label for="text_ru">Текст (русский)</label>
                    <textarea class="form-control" name="text_ru" id="text_ru" >{{$header->text_ru}}</textarea>
                </div>
                <div class="form-group">
                    <label for="text_ua">Текст (українська)</label>
                    <textarea class="form-control" name="text_ua" id="text_ua">{{$header->text_ua}}</textarea>
                </div>
                <div class="form-group">
                    <label for="description_ru">Описание для popup (русский)</label>
                    <textarea class="form-control" name="description_ru" id="description_ru" >{{$header->description_ru}}</textarea>
                </div>
                <div class="form-group">
                    <label for="description_ua">Опис для popup (українська)</label>
                    <textarea class="form-control" name="description_ua" id="description_ua" >{{$header->description_ua}}</textarea>
                </div>
                <div class="form-group">
                    <label for="address_ru">Адресс (русский)</label>
                    <input class="form-control" name="address_ru" id="address_ru" value="{{$header->address_ru}}">
                </div>
                <div class="form-group">
                    <label for="address_ua">Адресa (українська)</label>
                    <input class="form-control" name="address_ua" id="address_ua" value="{{$header->address_ua}}">
                </div>
                <div class="form-group">
                    <label for="phone">Номер телефона</label>
                    <input type="text" class="form-control" name="phone" id="phone" value="{{$header->phone}}">
                </div>
                <div class="form-group">
                    <label for="image">Выбрать изображение</label>
                    <input type="file" class="form-control" name="image[]" id="image" multiple>
                    @if ($errors->has('image'))
                    <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-default pull-right">Сохранить</button>
            </form>
        @if($header_images)
        @foreach($header_images as $image)
        <div class="img_block" style="float: left; margin: 10px;">
            <img title="Просмотреть" width="130" src='/images/header/{{$image->image}}'>
            <a href='/admin/delete_header_image/{{$image->id}}' type="button" class="btn btn-default btn-xs delete_btn" ><i class="fa fa-times" aria-hidden="true"></i></a>
        </div>
        @endforeach
        @endif
    </div>
</div>
@endsection