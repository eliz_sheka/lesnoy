<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header  pull-right">
        <a class="navbar-brand" href="index.html">Лесной</a>
    </div>


    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Главная</a>
                </li>
                <li>
                    <a href="/admin/rooms"><i class="fa fa-wrench fa-fw"></i> Номера</a>
                </li>
                <li>
                    <a href="/admin/restaurant"><i class="fa fa-table fa-fw"></i> Ресторан</a>
                </li>
                <li>
                    <a href="/admin/pastime"><i class="fa fa-edit fa-fw"></i> Развлечения</a>
                </li>
                <li>
                    <a href="/admin/kids"><i class="fa fa-edit fa-fw"></i> Для детей</a>
                </li>
                <li>
                    <a href="/admin/gallery"><i class="fa fa-edit fa-fw"></i> Галерея</a>
                </li>
                <li>
                    <a href="/admin/contact"><i class="fa fa-edit fa-fw"></i> Контакты</a>
                </li>
                <li>
                    <a href="/admin/footer"><i class="fa fa-edit fa-fw"></i> Футер</a>
                </li>

            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>