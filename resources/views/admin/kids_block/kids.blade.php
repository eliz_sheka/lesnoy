@extends('admin.app')
@section('content')
<div class="row">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Для детей <small style="font-size: 14px;" class="text-success">{{ session('success') }}</small></h1>
        </div>

        @if(empty($kids))
        <button data-toggle="collapse" data-target="#demo" class="btn btn-default add_btn">Добавить</button>
        @endif
        @if($kids)
        <a type="button" href="/admin/edit_kids" class="btn btn-default add_btn">Редактировать</a>
        @endif
        <div class="row collapse add_header" id="demo">
            <div class="col-sm-8">
                <form method="post" action="/admin/create_kids" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="title_ru">Заголовок (русский)</label>
                        <input required value="{{ old('title_ru') }}" type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Заголовок (русский)">
                        @if ($errors->has('title_ru'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('title_ru') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="title_ua">Заголовок (українська)</label>
                        <input required value="{{ old('title_ua') }}" type="text" class="form-control" name="title_ua" id="title_ua" placeholder="Заголовок (українська)">
                        @if ($errors->has('title_ua'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('title_ua') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="text_ru">Текст (русский)</label>
                        <textarea required rows="5" class="form-control" name="text_ru" id="text_ru" placeholder="Текст (русский)"></textarea>
                        @if ($errors->has('text_ru'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('text_ru') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="text_ua">Текст (українська)</label>
                        <textarea required rows="5" class="form-control" name="text_ua" id="text_ua" placeholder="Текст (українська)"></textarea>
                        @if ($errors->has('text_ua'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('text_ua') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="description_ru"><input type="checkbox" name="visible">Скрыть</label>
                    </div>
                    <div class="form-group">
                        <label for="image">Выбрать изображения</label>
                        <input type="file" class="form-control" name="image[]" id="image" multiple>
                        @if ($errors->has('image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-default pull-right">Сохранить</button>
                </form>
            </div>
        </div>
    </div>

    @if(!empty($kids))
    <div class="row header_table col-sm-10">
                <table class="table ">
                    <thead>
                        <tr >
                            <th>Українська</th>
                            <th>Русский</th>
                            <th style="text-align: center">Статус</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="border-right: 1px solid #ddd;">{{$kids->title_ua}}</td>
                            <td style="border-right: 1px solid #ddd;">{{$kids->title_ru}}</td>
                            <td rowspan="2" style="text-align: center; vertical-align: middle;">
                                @if(($kids->visible)==false) Скрыто @endif
                                @if(($kids->visible)==true) Показано @endif
                            </td>
                        </tr>
                        <tr>
                            <td style="border-right: 1px solid #ddd;">{{$kids->text_ua}}</td>
                            <td style="border-right: 1px solid #ddd;">{{$kids->text_ru}}</td>
                        </tr>
                    </tbody>
                </table>
        @if($kids_image)
        @foreach($kids_image as $image)
        <div class="col-sm-4" style="margin-bottom: 10px">
            <img width="200" src="/images/kids/{{$image->image}}">
        </div>
        @endforeach
        @endif
    </div>
    @endif
</div>
@endsection