@extends('admin.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование</h1>
    </div>
    <div class="row header_table col-sm-10">
        <form method="post" action="/admin/update_rooms/{{$room->id}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name_ua">Назва (українська)</label>
                <input required value="{{ $room->name_ua }}" type="text" class="form-control" name="name_ua" id="name_ua" placeholder="Название (українська)">
                @if ($errors->has('name_ua'))
                <span class="help-block">
                                <strong>{{ $errors->first('name_ua') }}</strong>
                            </span>
                @endif
            </div>
            <div class="form-group">
                <label for="name_ru">Название (русский)</label>
                <input required value="{{ $room->name_ru }}" type="text" class="form-control" name="name_ru" id="name_ru" placeholder="Название (русский)">
                @if ($errors->has('name_ru'))
                <span class="help-block">
                                <strong>{{ $errors->first('name_ru') }}</strong>
                            </span>
                @endif
            </div>
            <div class="form-group">
                <label for="info_ua">Текст (українська)</label>
                <textarea required rows="5" class="form-control" name="info_ua" id="info_ua" placeholder="Текст (українська)">{{$room->info_ua}}</textarea>
                @if ($errors->has('info_ua'))
                <span class="help-block">
                                        <strong>{{ $errors->first('info_ua') }}</strong>
                                    </span>
                @endif
            </div>
            <script>
                CKEDITOR.replace( 'info_ua' );
            </script>
            <div class="form-group">
                <label for="info_ru">Текст (русский)</label>
                <textarea required rows="5" class="form-control" name="info_ru" id="info_ru" placeholder="Текст (русский)">{{$room->info_ru}}</textarea>
                @if ($errors->has('info_ru'))
                <span class="help-block">
                                        <strong>{{ $errors->first('info_ru') }}</strong>
                                    </span>
                @endif
            </div>
            <script>
                CKEDITOR.replace( 'info_ru' );
            </script>
            <div class="form-group">
                <label for="image">Выбрать изображение</label>
                <input type="file" class="form-control" name="image[]" id="image" multiple>
                @if ($errors->has('image'))
                <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                @endif
            </div>
            @if($room->images)
            @foreach($room->images as $image)
            <div class="img_block" style="float: left; margin: 10px;">
                <img title="Просмотреть" width="130" src='/images/rooms/{{$image->image}}'>
                <a href='/admin/delete_room_image/{{$image->id}}' type="button" class="btn btn-default btn-xs delete_btn" ><i class="fa fa-times" aria-hidden="true"></i></a>
            </div>
            @endforeach
            @endif
            <button type="submit" class="btn btn-default pull-right">Сохранить</button>
        </form>
    </div>
</div>
@endsection