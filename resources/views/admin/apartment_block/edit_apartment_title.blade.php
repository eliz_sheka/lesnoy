@extends('admin.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование</h1>
    </div>
    <div class="row header_table col-sm-10">
        <form method="post" action="/admin/update_apartment_title/{{$apartment->id}}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title_ru">Текст (русский)</label>
                <input required value="{{ $apartment->title_ru }}" type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Текст (русский)">
                @if ($errors->has('title_ru'))
                <span class="help-block">
                                            <strong>{{ $errors->first('title_ru') }}</strong>
                                        </span>
                @endif
            </div>
            <div class="form-group">
                <label for="title_ua">Текст (українська)</label>
                <input required value="{{ $apartment->title_ua }}" type="text" class="form-control" name="title_ua" id="title_ua" placeholder="Текст (українська)">
                @if ($errors->has('title_ua'))
                <span class="help-block">
                                            <strong>{{ $errors->first('title_ua') }}</strong>
                                        </span>
                @endif
            </div>
            <button type="submit" class="btn btn-default pull-right">Сохранить</button>
        </form>
    </div>
</div>
@endsection