@extends('admin.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Номера <small style="font-size: 14px;" class="text-success">{{ session('success') }}</small></h1>
    </div>
    <div class="col-md-11 col-xs-12">
        @if(empty($apartment))
        <button data-toggle="collapse" data-target="#demo" class="btn btn-default add_btn" style="margin-bottom: 5px">Добавить заголовок</button>
        @endif
        @if($apartment)
        <a type="button" href="/admin/edit_apartment_title" class="btn btn-default add_btn">Редактировать заголовок</a>
        @endif
        <div class="row collapse add_header" id="demo">
            <div class="col-sm-12">
                <form method="post" action="/admin/create_apartment_title">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="title_ru">Заголовок (русский)</label>
                        <input required value="{{ old('title_ru') }}" type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Заголовок (русский)">
                        @if ($errors->has('title_ru'))
                        <span class="help-block">
                                            <strong>{{ $errors->first('title_ru') }}</strong>
                                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="title_ua">Заголовок (українська)</label>
                        <input required value="{{ old('title_ua') }}" type="text" class="form-control" name="title_ua" id="title_ua" placeholder="Заголовок (українська)">
                        @if ($errors->has('title_ua'))
                        <span class="help-block">
                                            <strong>{{ $errors->first('title_ua') }}</strong>
                                        </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-default pull-right">Сохранить</button>
                </form>
            </div>
        </div>
        @if(!empty($apartment))
        <div class="header_table col-sm-12">
            <table class="table ">
                <tbody>
                <tr>
                    <th>Українська</th>
                    <td>{{$apartment->title_ua}}</td>
                </tr>
                <tr>
                    <th>Русский</th>
                    <td>{{$apartment->title_ru}}</td>
                </tr>

                </tbody>
            </table>
        </div>
        @endif
    </div>
    <div class="col-md-11 col-xs-12">
        <button data-toggle="collapse" data-target="#demo1" class="btn btn-default add_btn">Добавить номер</button>
        <div class="row collapse add_header" id="demo1">
            <div class="col-sm-12">
                <form method="post" action="/admin/create_rooms" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name_ua">Назва (українська)</label>
                        <input required value="{{ old('name_ua') }}" type="text" class="form-control" name="name_ua" id="name_ua" placeholder="Название (українська)">
                        @if ($errors->has('name_ua'))
                        <span class="help-block">
                                <strong>{{ $errors->first('name_ua') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="name_ru">Название (русский)</label>
                        <input required value="{{ old('name_ru') }}" type="text" class="form-control" name="name_ru" id="name_ru" placeholder="Название (русский)">
                        @if ($errors->has('name_ru'))
                        <span class="help-block">
                                <strong>{{ $errors->first('name_ru') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="info_ua">Текст (українська)</label>
                        <textarea required rows="5" class="form-control" name="info_ua" id="info_ua" placeholder="Текст (українська)"></textarea>
                        @if ($errors->has('info_ua'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('info_ua') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <script>
                        CKEDITOR.replace( 'info_ua' );
                    </script>
                    <div class="form-group">
                        <label for="info_ru">Текст (русский)</label>
                        <textarea required rows="5" class="form-control" name="info_ru" id="info_ru" placeholder="Текст (русский)"></textarea>
                        @if ($errors->has('info_ru'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('info_ru') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <script>
                        CKEDITOR.replace( 'info_ru' );
                    </script>
                    <div class="form-group">
                        <label for="image">Выбрать изображения</label>
                        <input type="file" class="form-control" name="image[]" id="image" multiple>
                        @if ($errors->has('image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-default pull-right">Сохранить</button>
                </form>
            </div>
        </div>
        @if(!empty($rooms))
        <div class="row header_table">
            <table class="table ">
                <thead>
                    <tr >
                        <th class="col-sm-5" style="text-align: center;">Українська</th>
                        <th class="col-sm-5" style="text-align: center;">Русский</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($rooms as $room)
                <tr>
                    <td class="col-sm-5" style="border-right: 1px solid #ddd;">
                        <b>{{$room->name_ua}}</b><br>
                        {!!$room->info_ua!!}
                    </td>
                    <td class="col-sm-5" style="border-right: 1px solid #ddd;">
                        <b>{{$room->name_ru}}</b><br>
                        {!!$room->info_ru!!}
                    </td>
                    <td><a title="Редактировать" href="/admin/edit_room/{{$room->id}}"><i class="fa fa-pencil"></i></a></td>
                    <td><a title="Удалить" href="/admin/delete_room/{{$room->id}}"><i class="fa fa-times"></i></a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @endif
    </div>
</div>

@endsection