@extends('admin.app')
@section('content')
<div class="row">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Галерея <small style="font-size: 14px;" class="text-success">{{ session('success') }}</small></h1>
        </div>

        @if(empty($gallery))
        <button data-toggle="collapse" data-target="#demo" class="btn btn-default add_btn">Добавить</button>
        @endif
        @if($gallery)
        <a type="button" href="/admin/edit_gallery" class="btn btn-default add_btn">Редактировать</a>
        @endif
        <div class="row collapse add_header" id="demo">
            <div class="col-sm-8">
                <form method="post" action="/admin/create_gallery" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="title_ru">Заголовок (русский)</label>
                        <input required value="{{ old('title_ru') }}" type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Заголовок (русский)">
                        @if ($errors->has('title_ru'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('title_ru') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="title_ua">Заголовок (українська)</label>
                        <input required value="{{ old('title_ua') }}" type="text" class="form-control" name="title_ua" id="title_ua" placeholder="Заголовок (українська)">
                        @if ($errors->has('title_ua'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('title_ua') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="image">Выбрать изображения</label>
                        <input type="file" class="form-control" name="image[]" id="image" multiple>
                        @if ($errors->has('image'))
                        <span class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-default pull-right">Сохранить</button>
                </form>
            </div>
        </div>
    </div>

    @if(!empty($gallery))
    <div class="row header_table col-sm-10">
        <table class="table ">
            <thead>
            <tr>
                <th class="col-sm-6">Українська</th>
                <th class="col-sm-6">Русский</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="col-sm-6" style="border-right: 1px solid #ddd;">{{$gallery->title_ua}}</td>
                <td class="col-sm-6">{{$gallery->title_ru}}</td>
            </tr>
            </tbody>
        </table>
        @if($gallery_image)
        @foreach($gallery_image as $image)
        <div class="col-sm-4" style="margin-bottom: 10px">
            <img width="200" src="/images/gallery/{{$image->image}}">
        </div>
        @endforeach
        @endif
    </div>
    @endif
</div>
@endsection