@extends('admin.app')
@section('content')
<div class="row">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Развлечения <small style="font-size: 14px;" class="text-success">{{ session('success') }}</small></h1>
        </div>

        @if(empty($pastime))
        <button data-toggle="collapse" data-target="#demo" class="btn btn-default" style="margin-bottom: 5px; margin-left: 30px;">Добавить</button>
        @endif
        @if($pastime)
        <a type="button" href="/admin/edit_pastime" class="btn btn-default add_btn">Редактировать текст</a>
        @endif
        <div class="row collapse add_header" id="demo">
            <div class="col-sm-8">
                <form method="post" action="/admin/create_pastime">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="title_ru">Заголовок (русский)</label>
                        <input required value="{{ old('title_ru') }}" type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Заголовок (русский)">
                        @if ($errors->has('title_ru'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('title_ru') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="title_ua">Заголовок (українська)</label>
                        <input required value="{{ old('title_ua') }}" type="text" class="form-control" name="title_ua" id="title_ua" placeholder="Заголовок (українська)">
                        @if ($errors->has('title_ua'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('title_ua') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="text_ru">Текст (русский)</label>
                        <textarea rows="5" class="form-control" name="text_ru" id="text_ru" placeholder="Текст (русский)"></textarea>
                        @if ($errors->has('text_ru'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('text_ru') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="text_ua">Текст (українська)</label>
                        <textarea rows="5" class="form-control" name="text_ua" id="text_ua" placeholder="Текст (українська)"></textarea>
                        @if ($errors->has('text_ua'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('text_ua') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-default pull-right">Сохранить</button>
                </form>
            </div>
        </div>
    </div>

    @if(!empty($pastime))
    <div class="row header_table col-sm-11">
        <table class="table ">
            <thead>
                <tr>
                    <th class="col-sm-6">Українська</th>
                    <th class="col-sm-6">Русский</th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <td class="col-sm-6" style="border-right: 1px solid #ddd;">{{$pastime->title_ua}}</td>
                <td class="col-sm-6">{{$pastime->title_ru}}</td>
            </tr>
            <tr>
                <td class="col-sm-6" style="border-right: 1px solid #ddd;">{{$pastime->text_ua}}</td>
                <td class="col-sm-6">{{$pastime->text_ru}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    @endif


<div class="col-md-11 col-xs-12">
<button data-toggle="collapse" data-target="#demo1" class="btn btn-default">Добавить фото и подпись</button>
<div class="row collapse add_header" id="demo1" style="margin-left: 0;">
    <div class="col-sm-8">
        <form method="post" action="/admin/create_pastime_image" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title_ru">Заголовок (русский)</label>
                <input required value="{{ old('title_ru') }}" type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Заголовок (русский)">
                @if ($errors->has('title_ru'))
                <span class="help-block">
                                        <strong>{{ $errors->first('title_ru') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="title_ua">Заголовок (українська)</label>
                <input required value="{{ old('title_ua') }}" type="text" class="form-control" name="title_ua" id="title_ua" placeholder="Заголовок (українська)">
                @if ($errors->has('title_ua'))
                <span class="help-block">
                                        <strong>{{ $errors->first('title_ua') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="image">Выбрать изображение</label>
                <input type="file" class="form-control" name="image" id="image">
                @if ($errors->has('image'))
                <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
                @endif
            </div>
            <button type="submit" class="btn btn-default pull-right">Сохранить</button>
        </form>
    </div>
</div>


@if(!empty($pastime_images))
<div class="row header_table col-sm-11">
    <table class="table ">
        <thead>
        <th>Українська</th>
        <th>Русский</th>
        </thead>
        <tbody>
        @foreach($pastime_images as $pastime)
            <tr>
                <td class="col-sm-6" >{{$pastime->title_ua}}</td>
                <td class="col-sm-6">{{$pastime->title_ru}}</td>
                <td colspan="2" style="border-right: 1px solid #ddd; vertical-align: middle"><img width="140" src="/images/pastime/{{$pastime->image}}"></td>
                <td style="vertical-align: middle"><a href="/admin/edit_pastime_image/{{$pastime->id}}"><i class="fa fa-pencil"></i></a></td>
                <td style="vertical-align: middle"><a href="/admin/delete_pastime/{{$pastime->id}}"><i class="fa fa-times"></i></a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif
</div>
</div>
@endsection