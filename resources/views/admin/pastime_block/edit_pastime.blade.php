@extends('admin.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование</h1>
    </div>
    <div class="row header_table col-sm-10">
        <form method="post" action="/admin/update_pastime/{{$pastime->id}}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title_ru">Заголовок (русский)</label>
                <input required value="{{$pastime->title_ru }}" type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Заголовок (русский)">
                @if ($errors->has('title_ru'))
                <span class="help-block">
                    <strong>{{ $errors->first('title_ru') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="title_ua">Заголовок (українська)</label>
                <input required value="{{$pastime->title_ua }}" type="text" class="form-control" name="title_ua" id="title_ua" placeholder="Заголовок (українська)">
                @if ($errors->has('title_ua'))
                <span class="help-block">
                    <strong>{{ $errors->first('title_ua') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="text_ru">Текст (русский)</label>
                <textarea rows="5" class="form-control" name="text_ru" id="text_ru" placeholder="Текст (русский)">{{$pastime->text_ru}}</textarea>
                @if ($errors->has('text_ru'))
                <span class="help-block">
                    <strong>{{ $errors->first('text_ru') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="text_ua">Текст (українська)</label>
                <textarea rows="5" class="form-control" name="text_ua" id="text_ua" placeholder="Текст (українська)">{{$pastime->text_ua}}</textarea>
                @if ($errors->has('text_ua'))
                <span class="help-block">
                    <strong>{{ $errors->first('text_ua') }}</strong>
                </span>
                @endif
            </div>
            <button type="submit" class="btn btn-default pull-right">Сохранить</button>
        </form>
    </div>
</div>
@endsection