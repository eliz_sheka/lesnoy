@extends('admin.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование</h1>
    </div>
    <div class="row header_table col-sm-10">
        <form method="post" action="/admin/update_pastime_image/{{$pastime_image->id}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title_ru">Заголовок (русский)</label>
                <input required value="{{$pastime_image->title_ru }}" type="text" class="form-control" name="title_ru" id="title_ru" placeholder="Заголовок (русский)">
                @if ($errors->has('title_ru'))
                <span class="help-block">
                                        <strong>{{ $errors->first('title_ru') }}</strong>
                                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="title_ua">Заголовок (українська)</label>
                <input required value="{{ $pastime_image->title_ua }}" type="text" class="form-control" name="title_ua" id="title_ua" placeholder="Заголовок (українська)">
                @if ($errors->has('title_ua'))
                <span class="help-block">
                                        <strong>{{ $errors->first('title_ua') }}</strong>
                                    </span>
                @endif
            </div>
            @if($pastime_image->image)
            <div class="img_block">
                <img title="Просмотреть" width="130" src='/images/pastime/{{$pastime_image->image}}'>
                <a href='/admin/delete_pastime_image/{{$pastime_image->id}}' type="button" class="btn btn-default btn-xs delete_btn" ><i class="fa fa-times" aria-hidden="true"></i></a>
            </div>
            @endif
            @if(!$pastime_image->image)
            <div class="form-group">
                <label for="image">Выбрать изображение</label>
                <input type="file" class="form-control" name="image" id="image">
                @if ($errors->has('image'))
                <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
                @endif
            </div>
            @endif
            <button type="submit" class="btn btn-default pull-right">Сохранить</button>
        </form>
    </div>
</div>
@endsection