@extends('admin.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Редактирование</h1>
    </div>
    <div class="row header_table col-sm-10">
        <form method="post" action="/admin/update_contact/{{$contact->id}}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="phone">Номер телефона</label>
                <input required value="{{ $contact->phone }}" type="text" class="form-control" name="phone" id="phone" placeholder="Номер телефона">
                @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="phone2">Номер телефона 2</label>
                <input value="{{ $contact->phone2 }}" type="text" class="form-control" name="phone2" id="phone2" placeholder="Номер телефона 2">
                @if ($errors->has('phone2'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone2') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="email">E-mail</label>
                <input required value="{{ $contact->email }}" type="text" class="form-control" name="email" id="email" placeholder="E-mail">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="address_ru">Адресс (русский)</label>
                <input required value="{{ $contact->address_ru }}" class="form-control" name="address_ru" id="address_ru" placeholder="Адресс (русский)">
                @if ($errors->has('address_ru'))
                    <span class="help-block">
                        <strong>{{ $errors->first('address_ru') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="address_ua">Адресa (українська)</label>
                <input required value="{{ $contact->address_ua }}" class="form-control" name="address_ua" id="address_ua" placeholder="Адресa (українська)">
                @if ($errors->has('address_ua'))
                    <span class="help-block">
                        <strong>{{ $errors->first('address_ua') }}</strong>
                    </span>
                @endif
            </div>
            <button type="submit" class="btn btn-default pull-right">Сохранить</button>
        </form>
    </div>
</div>
@endsection