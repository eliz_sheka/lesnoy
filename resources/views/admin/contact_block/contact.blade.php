@extends('admin.app')
@section('content')
<div class="row">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Контакты <small style="font-size: 14px;" class="text-success">{{ session('success') }}</small></h1>
        </div>

        @if(empty($contact))
        <button data-toggle="collapse" data-target="#demo" class="btn btn-default add_btn">Добавить</button>
        @endif
        @if($contact)
        <a type="button" href="/admin/edit_contact" class="btn btn-default add_btn">Редактировать</a>
        @endif
        <div class="row collapse add_header" id="demo">
            <div class="col-sm-8">
                <form method="post" action="/admin/create_contact">
                    {{ csrf_field() }}
                    <div class="form-group" id="first_number" style="position: relative;">
                        <label for="phone">Номер телефона</label>
                        <button style="position: absolute; right: 0; bottom: 0;" title="Добавить номер" class="btn btn-default col-sm-1 add_phone_btn" type="button"><i class="fa fa-plus"></i></button>
                        <input required style="width: 90%;" value="{{ old('phone') }}" type="text" class="form-control " name="phone" id="phone" placeholder="Номер телефона">
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group display_none" id="second_number" style="position: relative;">
                        <label for="phone2">Номер телефона 2</label>
                        <button style="position: absolute; right: 0; bottom: 0;" title="Убрать номер" class="btn btn-default col-sm-1 remove_phone_btn" type="button"><i class="fa fa-minus"></i></button>
                        <input style="width: 90%" value="{{ old('phone2') }}" type="text" class="form-control" name="phone2" id="phone2" placeholder="Номер телефона 2">
                        @if ($errors->has('phone2'))
                        <span class="help-block">
                                <strong>{{ $errors->first('phone2') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input required value="{{ old('email') }}" type="text" class="form-control" name="email" id="email" placeholder="E-mail">
                        @if ($errors->has('email'))
                        <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="address_ru">Адресс (русский)</label>
                        <input required value="{{ old('address_ru') }}" class="form-control" name="address_ru" id="address_ru" placeholder="Адресс (русский)">
                        @if ($errors->has('address_ru'))
                        <span class="help-block">
                                <strong>{{ $errors->first('address_ru') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="address_ua">Адресa (українська)</label>
                        <input required value="{{ old('address_ua') }}" class="form-control" name="address_ua" id="address_ua" placeholder="Адресa (українська)">
                        @if ($errors->has('address_ua'))
                        <span class="help-block">
                                <strong>{{ $errors->first('address_ua') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-default pull-right">Сохранить</button>
                </form>
            </div>
        </div>
    </div>

    @if(!empty($contact))
    <div class="row header_table col-sm-10">
                <table class="table">
                    <tbody>
                    <tr>
                        <th>Номер телефона</th>
                        <td>{{$contact->phone}}</td>
                    </tr>
                    @if($contact->phone2 != null)
                    <tr>
                        <th>Номер телефона 2</th>
                        <td>{{$contact->phone2}}</td>
                    </tr>
                    @endif
                    <tr>
                        <th>E-mail</th>
                        <td>{{$contact->email}}</td>
                    </tr>
                    <tr>
                        <th>Адреса (українська)</th>
                        <td>{{$contact->address_ua}}</td>
                    </tr>
                    <tr>
                        <th>Адресс (русский)</th>
                        <td>{{$contact->address_ru}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
    @endif
</div>
@endsection