$(document).ready(function () {
    $('.add_phone_btn').click(function() {
        $('#second_number').removeClass('display_none');
        $(this).addClass('display_none');
        $('#first_number input').css('width', '100%');
    });
    $('.remove_phone_btn').click(function() {
        $('#first_number button').removeClass('display_none');
        $('#second_number').addClass('display_none');
        $('#first_number input').css('width', '90%');
    });

    $(window).bind("load", function() {
        $('#carousel a img').each(function() {
            if ($(this).css('z-index') == '5') {
                $data = $(this).data('description');
                $('.gallery_description').append($data);
            }
        });
    });

    $('.slide-prev').click(function () {
        $carousel = $(this).parent('.se-slider-box.hidden-xs').children('#carousel').children('a').children('img');

        $($carousel).each(function() {
            if ($(this).css('z-index') == '5') {
                $data = $(this).data('description');
                $('.gallery_description').empty();
                $('.gallery_description').append($data);
            }
        });
    });
    $('.slide-next').click(function () {
        $carousel = $(this).parent('.se-slider-box.hidden-xs').children('#carousel').children('a').children('img');

        $($carousel).each(function() {
            if ($(this).css('z-index') == '5') {
                $data = $(this).data('description');
                $('.gallery_description').empty();
                $('.gallery_description').append($data);
            }
        });


    });
});