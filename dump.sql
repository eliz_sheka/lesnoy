-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: lesnoy
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apartments`
--

DROP TABLE IF EXISTS `apartments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apartments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_ua` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apartments`
--

LOCK TABLES `apartments` WRITE;
/*!40000 ALTER TABLE `apartments` DISABLE KEYS */;
INSERT INTO `apartments` VALUES (1,'34 затишних номери','34 уютных номера','2017-07-31 18:19:44','2017-07-31 18:19:44');
/*!40000 ALTER TABLE `apartments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_ua` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'+38 050-633-40-45','+38066345556','admin@admin.com','Харьковская область, Красноградский район, с. Натальино, ул. И. Сенченка, 124','Харьковская область, Красноградский район, с. Натальино, ул. И. Сенченка, 124','2017-07-31 18:37:31','2017-08-01 06:09:34');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `footer_texts`
--

DROP TABLE IF EXISTS `footer_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `footer_texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text_ua` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `footer_texts`
--

LOCK TABLES `footer_texts` WRITE;
/*!40000 ALTER TABLE `footer_texts` DISABLE KEYS */;
INSERT INTO `footer_texts` VALUES (1,'© 2017 “Лесной” Готельно-ресторанний комплекс','© 2017 “Лесной” Гостинично-ресторанный комплекс','2017-07-31 18:37:53','2017-07-31 18:37:53');
/*!40000 ALTER TABLE `footer_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_ua` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (1,'Галерея комплексу','Галерея комплекса','2017-07-31 18:37:15','2017-07-31 18:37:15');
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery_images`
--

DROP TABLE IF EXISTS `gallery_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery_images`
--

LOCK TABLES `gallery_images` WRITE;
/*!40000 ALTER TABLE `gallery_images` DISABLE KEYS */;
INSERT INTO `gallery_images` VALUES (9,'15015346532afca29c74fcfa654795f00beda74fde5958ac11.jpg','2017-07-31 20:57:33','2017-07-31 20:57:33'),(11,'15015346533ba6adfdc2634957c94a0c4ede4c5f1639d7cb29.jpg','2017-07-31 20:57:33','2017-07-31 20:57:33'),(13,'1501534653cbb1d3d9dee0a670846919e35f848e93383e6317.jpg','2017-07-31 20:57:33','2017-07-31 20:57:33'),(14,'1501534709e7abe6c37b9dd7e4848b3383785fb9002b2a7131.jpg','2017-07-31 20:58:29','2017-07-31 20:58:29'),(15,'15015347094f06a4d86eafa912d7b0f64fa619b20cbed3045b.jpg','2017-07-31 20:58:29','2017-07-31 20:58:29'),(16,'150153470973f48b2ca667b2290b4b948833c221bd86a07bf7.jpg','2017-07-31 20:58:29','2017-07-31 20:58:29');
/*!40000 ALTER TABLE `gallery_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `header_images`
--

DROP TABLE IF EXISTS `header_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `header_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `header_images`
--

LOCK TABLES `header_images` WRITE;
/*!40000 ALTER TABLE `header_images` DISABLE KEYS */;
INSERT INTO `header_images` VALUES (1,'15015250309e546ed4b9c4124de6a1dff367dcd34830711c3e.jpg','2017-07-31 18:17:10','2017-07-31 18:17:10'),(2,'150152503015bdd21f8888f375c55a8b73ee4946d2a3c4d78d.jpg','2017-07-31 18:17:10','2017-07-31 18:17:10'),(3,'150152503035db93d67c63369bac4702981980cc4be2db3700.jpg','2017-07-31 18:17:10','2017-07-31 18:17:10');
/*!40000 ALTER TABLE `header_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `headers`
--

DROP TABLE IF EXISTS `headers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `headers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ua` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_ua` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_ua` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_ua` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `headers`
--

LOCK TABLES `headers` WRITE;
/*!40000 ALTER TABLE `headers` DISABLE KEYS */;
INSERT INTO `headers` VALUES (1,'«Лесной»','«Лесной»','Гостинично-ресторанный комплекс','Готельно-ресторанний комплекс','Отель «Лесной» предлагает 34 комфортабельных номера различных категорий. Они подойдут для остановки во время путешествия, делового визита, командировки и отдыха. Каждый номер имеет все необходимые удобства и условия комфорта для хорошего настроения, релакса и отдыха.\r\n\r\nДополнят отдых кафе и ресторан европейской и украинской кухни. Для ты, кто путешествует на собственном авто, в гостинице включены услуги парковки под охраной.\r\n\r\nОтель удобно расположен для отдыха в пути между дальними сочетаниями. Мы предлагаем номера с размещением от 1 до 4 человек с сервисом европейским стандартам.','Готель «Лесной» пропонує 34 комфортабельних номери різних категорій. Вони підійдуть для зупинки під час подорожі, ділового візиту, відрядження та відпочинку. Кожен номер має всі необхідні зручності та умови комфорту для гарного настрою, релаксу та відпочинку.\r\n\r\nДоповнять відпочинок кафе та ресторан європейської та української кухні. Для ти, хто подорожує власним авто , у готелі включені послуги парковки під охороною.\r\n\r\nГотель зручно розташований для відпочинку у дорозі між далекими сполученнями. Ми пропонуємо номери з розміщенням від 1 до 4 осіб з сервісом європейських стандартів.','Харьковская область, Красноградский район, с. Натальино, ул. И. Сенченка, 124','Харьковская область, Красноградский район, с. Натальино, ул. И. Сенченка, 124','+38 050-633-40-45','2017-07-31 18:17:10','2017-07-31 18:17:10');
/*!40000 ALTER TABLE `headers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kids`
--

DROP TABLE IF EXISTS `kids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kids` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_ua` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_ua` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kids`
--

LOCK TABLES `kids` WRITE;
/*!40000 ALTER TABLE `kids` DISABLE KEYS */;
INSERT INTO `kids` VALUES (1,'Для дітей','Для детей','Відпочинок з дітьми у нашому готелі для вас буде задоволенням.\r\n\r\nУ Вашому розпорядженні є прекрасні зони для відпочинку дітей та дитячі майданчики. Розміщення у номерах готелю сімей з дітьми також дуже комфортне. В повній мірі можна сказати, що ми створили справжній готель для відпочинку з дітьми.','Відпочинок з дітьми у нашому готелі для вас буде задоволенням.\r\n\r\nУ Вашому розпорядженні є прекрасні зони для відпочинку дітей та дитячі майданчики. Розміщення у номерах готелю сімей з дітьми також дуже комфортне. В повній мірі можна сказати, що ми створили справжній готель для відпочинку з дітьми.',1,'2017-07-31 18:35:30','2017-07-31 18:35:30');
/*!40000 ALTER TABLE `kids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kids_images`
--

DROP TABLE IF EXISTS `kids_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kids_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kids_images`
--

LOCK TABLES `kids_images` WRITE;
/*!40000 ALTER TABLE `kids_images` DISABLE KEYS */;
INSERT INTO `kids_images` VALUES (1,'1501526130936593494f384ff005eddc6e802b66d0e2c75989.jpg','2017-07-31 18:35:30','2017-07-31 18:35:30'),(2,'1501526130aa361cedc4053d187133b811beda2ceb7b2e324e.jpg','2017-07-31 18:35:31','2017-07-31 18:35:31'),(3,'15015261319f75e830100a924cbeb67d71c29652ccf835e39f.jpg','2017-07-31 18:35:31','2017-07-31 18:35:31');
/*!40000 ALTER TABLE `kids_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `links`
--

DROP TABLE IF EXISTS `links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `links`
--

LOCK TABLES `links` WRITE;
/*!40000 ALTER TABLE `links` DISABLE KEYS */;
INSERT INTO `links` VALUES (1,'Facebook','https://www.facebook.com','fa-facebook','2017-07-31 18:39:27','2017-07-31 18:39:27'),(2,'Twitter','https://twitter.com','fa-twitter','2017-07-31 18:39:44','2017-07-31 18:39:44');
/*!40000 ALTER TABLE `links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_07_24_122433_create_headers_table',1),(4,'2017_07_26_144514_create_contacts_table',1),(5,'2017_07_27_151318_create_links_table',1),(6,'2017_07_27_151400_create_footer_texts_table',1),(7,'2017_07_28_063615_create_apartments_table',1),(8,'2017_07_28_063630_create_rooms_table',1),(9,'2017_07_28_072208_create_kids_table',1),(10,'2017_07_28_081348_create_pastimes_table',1),(11,'2017_07_28_083830_create_restaurants_table',1),(12,'2017_07_28_083844_create_restaurant_items_table',1),(13,'2017_07_28_083930_create_galleries_table',1),(14,'2017_07_31_054107_create_pastime_images_table',1),(15,'2017_07_31_083057_create_gallery_images_table',1),(16,'2017_07_31_090729_create_kids_images_table',1),(17,'2017_07_31_090756_create_room_images_table',1),(18,'2017_07_31_100246_create_header_images_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pastime_images`
--

DROP TABLE IF EXISTS `pastime_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pastime_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_ua` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pastime_images`
--

LOCK TABLES `pastime_images` WRITE;
/*!40000 ALTER TABLE `pastime_images` DISABLE KEYS */;
INSERT INTO `pastime_images` VALUES (1,'Трекінг по лісовому масиву','Трекинг по лесному массиву','1501525961.jpg','2017-07-31 18:32:41','2017-07-31 18:32:41'),(3,'Галерея комплексу','Галерея комплекса','1501567235.jpg','2017-07-31 18:34:48','2017-08-01 06:00:35');
/*!40000 ALTER TABLE `pastime_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pastimes`
--

DROP TABLE IF EXISTS `pastimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pastimes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_ua` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_ua` text COLLATE utf8mb4_unicode_ci,
  `text_ru` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pastimes`
--

LOCK TABLES `pastimes` WRITE;
/*!40000 ALTER TABLE `pastimes` DISABLE KEYS */;
INSERT INTO `pastimes` VALUES (1,'Розваги та відпочинок','Развлечения и отдых','Російська баня/лазня очистить тіло й душу, розслабить та подарує відчуття свіжості. Продовжити відпочинок можна всередині лазні, або в альтанці. Це найкраще місце для романтичної вечері, барбекю з друзями та навіть ділової зустрічі.\r\n\r\nБільярдна допоможе весело провести час. Більярдний стіл, відточений кий, матові кулі та м’яке світло зроблять вечір особливим.','Російська баня/лазня очистить тіло й душу, розслабить та подарує відчуття свіжості. Продовжити відпочинок можна всередині лазні, або в альтанці. Це найкраще місце для романтичної вечері, барбекю з друзями та навіть ділової зустрічі.\r\n\r\nБільярдна допоможе весело провести час. Більярдний стіл, відточений кий, матові кулі та м’яке світло зроблять вечір особливим.','2017-07-31 18:32:21','2017-07-31 18:32:21');
/*!40000 ALTER TABLE `pastimes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_items`
--

DROP TABLE IF EXISTS `restaurant_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_ua` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_ua` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_items`
--

LOCK TABLES `restaurant_items` WRITE;
/*!40000 ALTER TABLE `restaurant_items` DISABLE KEYS */;
INSERT INTO `restaurant_items` VALUES (1,'Меню ресторана','Меню ресторана','<p>Сніданок, обід чи вечеря в затишній атмосфері ресторану та банкетної зали. Великий вибір страв європейської та української кухні на будь-який смак. Висококваліфікований персонал забезпечить дозвілля якісним сервісом. Ресторан вміщує 100 гостей, а банкетна зала пропонує 40 місць до ваших послуг.</p>','<p>Сніданок, обід чи вечеря в затишній атмосфері ресторану та банкетної зали. Великий вибір страв європейської та української кухні на будь-який смак. Висококваліфікований персонал забезпечить дозвілля якісним сервісом. Ресторан вміщує 100 гостей, а банкетна зала пропонує 40 місць до ваших послуг.</p>',1,'1501525632.png','2017-07-31 18:27:12','2017-07-31 18:27:12'),(2,'Готель приймає замовлення на:','Отель принимает заказы на:','<ul><li>Проведення банкетів, фуршетів</li><li>Організацію свят</li><li>Оренду залу для проведення семінарів, форумів, робочих зустрічей.</li></ul>','<ul><li>Проведение банкетов, фуршетов</li><li>организацию праздников</li><li>Аренду зала для проведения семинаров, форумов, рабочих встреч.</li></ul>',2,'1501525688.png','2017-07-31 18:28:08','2017-07-31 18:28:08'),(3,'Урочистості','Торжества','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui</p>','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui</p>',3,'1501525738.png','2017-07-31 18:28:58','2017-07-31 18:28:58');
/*!40000 ALTER TABLE `restaurant_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurants`
--

DROP TABLE IF EXISTS `restaurants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_ua` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_ua` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_ru` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_ua` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_ru` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_ua` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_ru` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurants`
--

LOCK TABLES `restaurants` WRITE;
/*!40000 ALTER TABLE `restaurants` DISABLE KEYS */;
INSERT INTO `restaurants` VALUES (1,'Ресторан європейської та української кухні','Ресторан европейской и украинской кухни','menu_ua.docx','menu_ru.docx','docx.png','docx.png','10','11','2017-07-31 18:25:57','2017-07-31 18:25:57');
/*!40000 ALTER TABLE `restaurants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_images`
--

DROP TABLE IF EXISTS `room_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_images`
--

LOCK TABLES `room_images` WRITE;
/*!40000 ALTER TABLE `room_images` DISABLE KEYS */;
INSERT INTO `room_images` VALUES (1,'1501525385765ee32caaac77d2ffbeba40a44d70c9e4a94a39.jpg',1,'2017-07-31 18:23:05','2017-07-31 18:23:05'),(2,'150152538564f8bc0637fa6f2286544ca1916bb29824cb26b9.jpg',1,'2017-07-31 18:23:05','2017-07-31 18:23:05'),(3,'15015254518060fc8ec09af55bde15d623a6a3c2b5a3b767a0.jpg',2,'2017-07-31 18:24:11','2017-07-31 18:24:11'),(4,'1501525451b5d8b0abb20f285aff7790efa65434fd92a640be.jpg',2,'2017-07-31 18:24:11','2017-07-31 18:24:11');
/*!40000 ALTER TABLE `room_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_ua` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ru` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info_ua` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `info_ru` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,'Люкс','Люкс','<ul><li>дерев&#39;яні ліжка</li><li>Диван-малютка</li><li>холодильник</li><li>електрочайник</li><li>телевізор</li><li>кондиціонер</li><li>санвузол</li><li>Гаряча вода</li><li>Тераса з пластиковими столом і лавками (3-місні)</li></ul>','<ul><li>Деревянные кровати</li><li>Диван-малютка</li><li>Холодильник</li><li>Электрочайник</li><li>Телевизор</li><li>Кондиционер</li><li>Санузел</li><li>Горячая вода</li><li>Терраса с пластиковыми столом и скамейками (3-хместные)</li></ul>','2017-07-31 18:23:05','2017-07-31 18:23:33'),(2,'Напівлюкс','Полулюкс','<ul><li>дерев&#39;яні ліжка</li><li>Диван-малютка</li><li>холодильник</li><li>електрочайник</li><li>телевізор</li><li>кондиціонер</li><li>санвузол</li><li>Гаряча вода</li><li>Тераса з пластиковими столом і лавками (3-місні)</li></ul>','<ul><li>Деревянные кровати</li><li>Раскладывающийся диван (в 4-х 5-ти местных)</li><li>Холодильник</li><li>Телевизор</li><li>Кондиционер (сплит-система)</li><li>Кухня</li><li>Эл.чайник, эл.плита, посуда</li><li>Терраса с пластиковыми столом и скамейками (в 4-х местных)</li></ul>','2017-07-31 18:24:11','2017-07-31 18:24:11');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','admin@admin.com','$2y$10$CAaFUZBHSom4i1axNgwEW.wIloo275ON/5Xcvw8OoGE3WwjXKAKUq',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-01  6:22:31
