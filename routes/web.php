<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index');
Route::get('/ua', 'PageController@index_ua');

Auth::routes();

//admin routes
Route::group(['prefix' => '/admin','middleware'=>'auth'], function () {
    Route::get('/', 'AdminController@index');
    Route::post('create_header', 'AdminController@create_header');
    Route::get('edit_header', 'AdminController@edit_header');
    Route::post('update_header/{id}', 'AdminController@update_header');
    Route::get('delete_header_image/{id}', 'AdminController@delete_image');

    Route::get('contact', 'AdminContactController@contact');
    Route::get('edit_contact', 'AdminContactController@edit_contact');
    Route::post('create_contact', 'AdminContactController@create_contact');
    Route::post('update_contact/{id}', 'AdminContactController@update_contact');

    Route::get('footer', 'AdminFooterController@footer');
    Route::get('edit_footer_text', 'AdminFooterController@edit_footer_text');
    Route::post('create_footer_text', 'AdminFooterController@create_footer_text');
    Route::post('update_footer_text/{id}', 'AdminFooterController@update_footer_text');

    Route::get('edit_link/{id}', 'AdminFooterController@edit_link');
    Route::post('create_link', 'AdminFooterController@create_link');
    Route::post('update_link/{id}', 'AdminFooterController@update_link');
    Route::get('delete_link/{id}', 'AdminFooterController@delete_link');

    Route::get('rooms', 'AdminApartmentController@rooms');
    Route::get('edit_room/{id}', 'AdminApartmentController@edit_room');
    Route::get('edit_apartment_title', 'AdminApartmentController@edit_apartment_title');
    Route::post('create_rooms', 'AdminApartmentController@create_rooms');
    Route::post('create_apartment_title', 'AdminApartmentController@create_apartment_title');
    Route::post('update_rooms/{id}', 'AdminApartmentController@update_rooms');
    Route::post('update_apartment_title/{id}', 'AdminApartmentController@update_apartment_title');
    Route::get('delete_room/{id}', 'AdminApartmentController@delete_room');
    Route::get('delete_room_image/{id}', 'AdminApartmentController@delete_image');

    Route::get('kids', 'AdminKidsController@kids');
    Route::post('create_kids', 'AdminKidsController@create_kids');
    Route::get('edit_kids', 'AdminKidsController@edit_kids');
    Route::post('update_kids/{id}', 'AdminKidsController@update_kids');
    Route::get('delete_kids_image/{id}', 'AdminKidsController@delete_image');

    Route::get('pastime', 'AdminPastimeController@pastime');
    Route::post('create_pastime', 'AdminPastimeController@create_pastime');
    Route::post('create_pastime_image', 'AdminPastimeController@create_pastime_image');
    Route::get('edit_pastime', 'AdminPastimeController@edit_pastime');
    Route::get('edit_pastime_image/{id}', 'AdminPastimeController@edit_pastime_image');
    Route::post('update_pastime/{id}', 'AdminPastimeController@update_pastime');
    Route::post('update_pastime_image/{id}', 'AdminPastimeController@update_pastime_image');
    Route::get('delete_pastime_image/{id}', 'AdminPastimeController@delete_pastime_image');
    Route::get('delete_pastime/{id}', 'AdminPastimeController@delete_pastime');

    Route::get('restaurant', 'AdminRestaurantController@restaurant');
    Route::get('edit_restaurant_item/{id}', 'AdminRestaurantController@edit_restaurant_item');
    Route::get('edit_restaurant_title', 'AdminRestaurantController@edit_restaurant_title');
    Route::post('create_restaurant_item', 'AdminRestaurantController@create_restaurant_item');
    Route::post('create_restaurant_title', 'AdminRestaurantController@create_restaurant_title');
    Route::post('update_restaurant_item/{id}', 'AdminRestaurantController@update_restaurant_item');
    Route::post('update_restaurant_title/{id}', 'AdminRestaurantController@update_restaurant_title');
    Route::get('delete_menu_ua/{id}', 'AdminRestaurantController@delete_menu_ua');
    Route::get('delete_menu_ru/{id}', 'AdminRestaurantController@delete_menu_ru');
    Route::get('delete_image/{id}', 'AdminRestaurantController@delete_image');

    Route::get('gallery', 'AdminGalleryController@gallery');
    Route::post('create_gallery', 'AdminGalleryController@create_gallery');
    Route::get('edit_gallery', 'AdminGalleryController@edit_gallery');
    Route::post('update_gallery/{id}', 'AdminGalleryController@update_gallery');
    Route::get('delete_gallery/{id}', 'AdminGalleryController@delete_image');

});

Route::get('/restaurant/menu/{name}', function($name){
    $path = storage_path('app/public/menu') . '/' . $name;
    if(!File::exists($path)) abort(404);
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});
Route::get('/images/{folder}/{name}', function($folder, $name){
    $path = storage_path('app/public/images/'.$folder) . '/' . $name;
    if(!File::exists($path)) abort(404);
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});
