<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_ua');
            $table->string('title_ru');
            $table->string('menu_ua')->nullable();
            $table->string('menu_ru')->nullable();
            $table->string('img_ua')->nullable();
            $table->string('img_ru')->nullable();
            $table->string('size_ua')->nullable();
            $table->string('size_ru')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
